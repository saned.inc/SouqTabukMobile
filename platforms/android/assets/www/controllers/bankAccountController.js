﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('bankAccountController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function LoadBankAccounts(callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        // app.showIndicator();
        appServices.CallService('bankAccount', 'GET', "api/v1/general/info", '', function (result) {
            app.hideIndicator();
            if (result && result.bank_accounts != null && result.bank_accounts.length > 0) {
                $$('.page[data-page="bankAccount"]').removeClass('noResult');
                $scope.noBankAccounts = typeof result.bank_accounts != 'undefined' && result.bank_accounts != null && result.bank_accounts.length > 0 ? false : true;
                $scope.BankAccounts = result.bank_accounts;
            }
            else {
                $$('.page[data-page="bankAccount"]').addClass('noResult');
                $scope.noBankAccounts = true;
            }

            callBack(true);

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $('#BankAccountNotificationsLoaded').hide();
    $('#BankAccountNotificationsNotLoaded').show();

    $(document).ready(function () {
        app.onPageInit('bankAccount', function (page) {
            if ($rootScope.currentOpeningPage != 'bankAccount') return;
            $rootScope.currentOpeningPage = 'bankAccount';

        });

        app.onPageBeforeAnimation('bankAccount', function (page) {
            if ($rootScope.currentOpeningPage != 'bankAccount') return;
            $rootScope.currentOpeningPage = 'bankAccount';

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#BankAccountNotificationsLoaded').show();
                    $('#BankAccountNotificationsNotLoaded').hide();
                });
            }

            $scope.isLoaded = false;

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            LoadBankAccounts(function (result) { });

            $rootScope.RemoveEditPagesFromHistory();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('bankAccount', function (page) {

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('bankAccount', function (page) {
            if ($rootScope.currentOpeningPage != 'bankAccount') return;
            $rootScope.currentOpeningPage = 'bankAccount';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true; cordova.plugins.notification.badge.clear();
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };

        app.init();
    });

}]);

