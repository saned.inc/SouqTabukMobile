﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupUserController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var citySelect;
    $rootScope.editUserProfileForm = {};

    function LoadCities(callBack) {
        var cities = [];

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
       // app.showIndicator();

        $('#linkSignupCity .item-after').html('اختر مدينتك');
        //$scope.userForm.city = null;

        appServices.CallService('signupUser', "GET", "api/v1/cities", '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {
                citySelect = {};
                //citySelect = res[0];
                //$scope.userForm.city = res[0];
                res.splice(0, 1);
                $scope.cities = res;
                myApp.fw7.Cities = res;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function OpenActivationCodePopup() {
        var activationSignup = app.modal({
            title: 'كود التفعيل',
            text: '',
            afterText: '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="number" name="phone" placeholder="كود التفعيل">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>',
            buttons: [
                {
                    text: 'تفعيل',
                    onClick: function () {
                        var code = $('#txtCode').val();
                        var mobile = CookieService.getCookie('confirmationMobile');

                        var params = {
                            'activation_code': code
                        };

                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                        //app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/v1/activation', params, function (res) {
                            app.hideIndicator();

                            if (res != null) {
                                language.openFrameworkModal('نجاح', 'تم التأكد من الجوال بنجاح .', 'alert', function () { });

                                app.closeModal(activationSignup);

                                CookieService.setCookie('appToken', res.api_token);
                                CookieService.setCookie('USName', res.username);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginUsingSocial', false);
                                CookieService.setCookie('UserEntersCode', 'true');
                                CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                                helpers.GoToPage('home', null);
                                
                            }
                            else {
                                language.openFrameworkModal('خطأ', 'كود التفعيل غير صحيح .', 'alert', function () {});
                                OpenActivationCodePopup();
                            }
                        });
                    }
                },
                {
                    text: 'إعادة إرسال الكود',
                    onClick: function () {
                        var mobile = CookieService.getCookie('confirmationMobile');

                        var params = {
                            'phone': mobile
                        };

                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                        //app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/v1/activation/resend', params, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () {});
                                app.openModal(activationSignup);
                            }
                            else {
                                app.openModal(activationSignup);
                            }
                        });
                    }
                },
                {
                    text: 'إلغاء',
                    onClick: function () {
                        app.closeModal(activationSignup);
                    }
                }
            ]
        })
    }

    $(document).ready(function () {
        app.onPageInit('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
        });

        app.onPageReinit('signupUser', function (page) {
            var fromPage = page.fromPage.name;
            if (fromPage != 'map') {
                $scope.resetForm();
            }
            
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageBeforeAnimation('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
            var fromPage = page.fromPage.name;
            if (fromPage != 'map') {
                $scope.resetForm();
                $rootScope.getLocation();
                LoadCities(function (cities) {

                });
            }
            $rootScope.RemoveEditPagesFromHistory();
            

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser'; $rootScope.getCurrentLocation

        });
        $rootScope.getLocation = function () {
             if (app.device.android) {
            $rootScope.locationLoading = true;
             cordova.plugins.locationAccuracy.canRequest(function (canRequest) {
                 if (canRequest == 1) {
                     cordova.plugins.locationAccuracy.request(function (success) {
                         $rootScope.locationLoading = false;
                     getCurrentLocation();
                     }, function (error) {
                         pulginService.errorMessage('من فضلك فم بفتح نظام تحديد المواقع (gps)  .', "bottom", 1000);
                         $rootScope.locationLoading = false;
                     }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
                 }
                 else {
                     $rootScope.locationLoading = false;
                     cordova.plugins.locationAccuracy.request(function (success) {
                         getCurrentLocation();
                     }, function (error) {
                         pulginService.errorMessage('من فضلك فم بفتح نظام تحديد المواقع (gps)  .', "bottom", 1000);
                     }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
                 }
             });
                }
                else {
                    $rootScope.getCurrentLocation();
                }

        }

        function getCurrentLocation () {
            var onSuccess = function (position) {
                var currentPlaceLatitude = position.coords.latitude;
                var currentPlaceLongitude = position.coords.longitude;
                if ($rootScope.latUpdate != null && $rootScope.lngUpdate != null) {
                    $rootScope.lat = $rootScope.latUpdate;
                    $rootScope.lng = $rootScope.lngUpdate;
                }
                else {
                    $rootScope.lat = currentPlaceLatitude;
                    $rootScope.lng = currentPlaceLongitude;
                }
                nativegeocoder.reverseGeocode(success, failure,parseFloat($rootScope.lat) ,parseFloat($rootScope.lng) , { defaultLocale: 'en' });
                function success(result) {
                    if (result[0]) {
                        var res = [];
                        res.push(result[0].countryName);
                        res.push(result[0].administrativeArea);
                        res.push(result[0].locality);
                        res.push(result[0].thoroughfare);
                        res = res.reverse();
                        $rootScope.address = res.toString()
                        $rootScope.editUserProfileForm.editAddress = res.toString()
                        $rootScope.signAddress = res.toString();
                        $rootScope.locationLoading = false;
                        setTimeout(function () {
                            $scope.$apply()
                        },500)
                    }
                }
                function failure(err) {
                    $rootScope.locationLoading = false;
                    setTimeout(function () {
                        $rootScope.$apply()
                    }, 500)
                }

            };
            var onError = function (positionError) {
                language.openFrameworkModal('خطأ', 'خطا في تحديد الموقع .', 'alert', function () { });
                $rootScope.locationLoading = false;
            };
            navigator.geolocation.getCurrentPosition(onSuccess, onError);
            
        }

      
        $scope.resetForm = function () {
            $scope.signUpUserReset = false;
            $scope.submittedUserSignup = false;
            $rootScope.lat = null;
            $rootScope.lng = null;
            $rootScope.latUpdate = null
            $rootScope.lngUpdate = null
            $scope.isCityValid = true;
            $rootScope.signAddress = null;
            $scope.userForm.username = null;
            $scope.userForm.email = null;
            $scope.userForm.mobile = null;
            $scope.userForm.country = null;
            $scope.userForm.city = null;
            $scope.userForm.newPassword = null;
            $scope.userForm.confrmNewPassword = null;

            $('#linkSignupCity .item-after').html('المدينة');

            $scope.user = {};
            if (typeof $scope.SignupUserForm != 'undefined' && $scope.SignupUserForm != null) {
                $scope.SignupUserForm.$setPristine(true);
                $scope.SignupUserForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (signUpForm) {
            $scope.signUpUserReset = true;
            $scope.submittedUserSignup = true;
            $scope.isCityValid = $scope.signUpUserReset && $scope.submittedUserSignup && signUpForm.city.$valid;

            //OpenActivationCodePopup();
         
            if (signUpForm.$valid) {
                $('#userSignUp').prop('disabled', 'disabled');
                var user = {
                    "name": $scope.userForm.username,
                    "username": $scope.userForm.username,
                    "email": $scope.userForm.email,
                    "phone": $scope.userForm.mobile,
                    "lat": $rootScope.lat,
                    "lng": $rootScope.lng,
                    "city": $scope.userForm.city.id,
                    "password": $scope.userForm.newPassword,
                    "password_confirmation": $scope.userForm.confrmNewPassword
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                //app.showIndicator();
                appServices.CallService('signupUser', "POST", "api/v1/register", user, function (res2) {
                    $('#userSignUp').prop('disabled', '');
                    app.hideIndicator();

                    if (res2 != null && !Array.isArray(res2)) {
                        CookieService.setCookie('confirmationMobile', $scope.userForm.mobile);
                        //CookieService.setCookie('appToken', res2.token);
                        CookieService.setCookie('UserEntersCode', false);
                        language.openFrameworkModal('نجاح', 'تم تسجيل بياناتك بنجاح .', 'alert', function () {});
                        OpenActivationCodePopup();
                    }
                    else if (typeof res2.length != 'undefined' && res2.length > 0) {

                        var errorsText = '<ul>';
                        angular.forEach(res2,function(error){
                            errorsText += '<li><p>' + error + '</p></li>';
                        });
                        errorsText += '</ul>';

                        var errorPopup = app.modal({
                            title: 'الأخطاء',
                            text: '',
                            afterText: '<div class="list-block">' +
                            '<div class="m-auto">' +
                            errorsText +
                            '</div>' +
                            '</div>',
                            buttons: [
                                {
                                    text: 'إغلاق',
                                    onClick: function () {
                                        app.closeModal(errorPopup);
                                    }
                                }
                            ]
                        });
                    }
                    else {

                    }
                });
            }
        };
        $scope.form = {};
        $scope.userForm = {};

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToLogin = function () {
            helpers.GoToPage('login', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();

    });
}]);

