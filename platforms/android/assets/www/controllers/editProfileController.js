﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('editProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var oldUserMobile = '';
    var newUserMobile = '';
    var cityId = '';
    var ft;
    var tempUserImage;

    function LoadCities(callBack) {
        var cities = [];

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        //app.showIndicator();

        $('#linkEditProfileCity .item-after').html('المدينة');
        $scope.editUserProfileForm.city = null;

        appServices.CallService('editProfile', "GET", "api/v1/cities", '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {
                $scope.cities = res;
                myApp.fw7.Cities = res;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function OpenActivationCodePopup() {
        var activation = app.modal({
            title: 'كود التفعيل',
            text: '',
            afterText: '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="number" name="phone" placeholder="كود التفعيل">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>',
            buttons: [
                {
                    text: 'تفعيل',
                    onClick: function () {
                        var code = $('#txtCode').val();
                        var mobile = CookieService.getCookie('confirmationMobile');

                        var params = {
                            'phone': mobile,
                            'activation_code': code
                        };

                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                       // app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/v1/activation/code', params, function (res) {
                            app.hideIndicator();
                            app.closeModal(activation);

                            if (res != null && res != 'mobileExistBefore') {
                                language.openFrameworkModal('نجاح', 'تم التأكد من الجوال بنجاح .', 'alert', function () {});
                                
                                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                                userLoggedIn.phone = mobile;
                                CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));
                                helpers.GoToPage('userProfile', null);
                            }
                            else if (res != null && res == 'mobileExistBefore') {
                                language.openFrameworkModal('خطأ', 'رقم الجوال موجود من قبل .', 'alert', function () { });
                                app.closeModal(activation);
                                var oldUserMobile = CookieService.getCookie('oldUserMobile');
                                $scope.editUserProfileForm.mobile = oldUserMobile;
                                setTimeout(function () {
                                    $scope.$apply();
                                }, fw7.DelayBeforeScopeApply);
                            }
                            else {
                                language.openFrameworkModal('خطأ', 'كود التفعيل غير صحيح .', 'alert', function () {});
                                OpenActivationCodePopup();
                            }
                        });
                    }
                },
                {
                    text: 'إعادة إرسال الكود',
                    onClick: function () {
                        var mobile = CookieService.getCookie('oldUserMobile');
                        var newUserMobile = CookieService.getCookie('confirmationMobile');

                        var params = {
                            'phone': mobile,
                            'new_phone': newUserMobile
                        };

                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                        //app.showIndicator();
                        appServices.CallService('editProfile', "POST", 'api/v1/activation/resend', params, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () {
                                    app.openModal(activation);
                                });
                            }
                            else {
                                app.openModal(activation);
                            }
                        });
                    }
                },
                {
                    text: 'إلغاء',
                    onClick: function () {
                        var mobile = CookieService.getCookie('oldUserMobile');
                        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        userLoggedIn.phone = mobile;
                        $scope.editUserProfileForm.mobile = mobile;
                        CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));

                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                        app.closeModal(activation);
                    }
                }
            ]
        });

    }

    function LoadUserData(user) {
        //$scope.editUserProfileForm.fullname = typeof user != 'undefined' && user != null ? user.name : false;
        $scope.editUserProfileForm.email = typeof user != 'undefined' && user.email != null ? user.email : '';
        $scope.editUserProfileForm.mobile = typeof user != 'undefined' && user.phone != null ? user.phone : '';
        $scope.editUserProfileForm.username = typeof user != 'undefined' && user != null ? user.username : '';

        oldUserMobile = $scope.editUserProfileForm.mobile;
        newUserMobile = $scope.editUserProfileForm.mobile;

        var filteredCities = [];

        filteredCities = myApp.fw7.Cities;

        $scope.cities = filteredCities;

        var cityName = 'المدينة';

        if (user.city_id != 'undefined' && user.city_id != null && user.city_id != ' ' && user.city_id != '') {
            
            angular.forEach(filteredCities, function (city) {
                if (city.id == user.city_id) {
                    cityName = city.name;
                    app.smartSelectAddOption('#linkEditProfileCity select', '<option value="' + city.id + '" selected>' + city.name + '</option>');
                }
                else {
                    app.smartSelectAddOption('#linkEditProfileCity select', '<option value="' + city.id + '">' + city.name + '</option>');
                }
            });
        }
        else {
            cityName = typeof user.city != 'undefined' && user.city != null ? user.city.name : 'المدينة';

            angular.forEach(filteredCities, function (city) {
                if (typeof user.city != 'undefined' && user.city != null && city.id == user.city.id) {
                    app.smartSelectAddOption('#linkEditProfileCity select', '<option value="' + city.id + '" selected>' + city.name + '</option>');
                }
                else {
                    app.smartSelectAddOption('#linkEditProfileCity select', '<option value="' + city.id + '">' + city.name + '</option>');
                }
            });
        }

        $('#linkEditProfileCity .item-after').text(cityName);
        if (user.city_id != 'undefined' && user.city_id != null && user.city_id != ' ' && user.city_id != '') {
            cityId = user.city_id;
            $('#linkEditProfileCity select').val(user.city_id);
        }
        else {
            if (typeof user.city != 'undefined' && user.city != null) {
                cityId = user.city.id;
                $('#linkEditProfileCity select').val(user.city.id);
            }
        }

        $$('#linkEditProfileCity select').on('change', function () {
            cityId = $(this).val();
        });

        if (typeof user.image != 'undefined' && user.image != null && user.image != '' && user.image != ' ') {
            $scope.userImage = user.image;
        }
        else {
            $scope.userImage = 'img/avatar.jpg';
        }

        $scope.userImageElement = {
            id: 1,
            loading: false,
            src: $scope.userImage,
            progress: 0
        };

        tempUserImage = $scope.userImageElement.src;

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $('#EditProfileNotificationsLoaded').hide();
    $('#EditProfileNotificationsNotLoaded').show();

    $(document).ready(function () {
        app.onPageInit('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

        });

        app.onPageBeforeAnimation('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';
            var fromPage = page.fromPage.name;
            var user = page.query.user;
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#EditProfileNotificationsLoaded').show();
                    $('#EditProfileNotificationsNotLoaded').hide();
                });
            }
            if (fromPage != 'map') {
                $scope.resetForm();
                $rootScope.getLocation();
                LoadCities(function (result) {
                    LoadUserData(userLoggedIn);
                });
            }

        
            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

         

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        //app.onPageBack('editProfile', function (page) {
        //    var fromPage = page.fromPage.name;
        //    if (fromPage != 'map') {
        //        $scope.resetForm();
               
        //    }
        //});

        app.onPageAfterAnimation('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

        });

        $scope.resetForm = function () {
            $scope.editUserProfileReset = false;
            $rootScope.editUserProfileForm.editAddress = null;
            $scope.submittedEditUserProfile = false;
            $rootScope.lat = null;
            $rootScope.lng = null;
            $scope.isCityValid = true;
            $scope.editUserProfileForm.username = null;
            //$scope.editUserProfileForm.fullname = null;
            $scope.editUserProfileForm.name = null;
            $scope.editUserProfileForm.email = null;
            $scope.editUserProfileForm.mobile = null;

            if (typeof $scope.EditUserProfileForm != 'undefined' && $scope.EditUserProfileForm != null) {
                $scope.EditUserProfileForm.$setPristine(true);
                $scope.EditUserProfileForm.$setUntouched();
            }
        }

        function ConvertImgToBase64Url(url, outputFormat, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                var dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL, url);
                canvas = null;
            };

            img.src = url;
        }

        function getImage() {

            window.imagePicker.getPictures(function (results) {
                if (results && results.length > 0) {

                    angular.forEach(results, function (image) {
                        var selectedImage = image;

                        $scope.userImageElement = {
                            id: 1,
                            loading: false,
                            src: $scope.userImage,
                            progress: 0
                        };

                        ConvertImgToBase64Url(selectedImage, 'jpg', function (imgBase64, selectedImage) {
                            $scope.userImage = imgBase64.split(',')[1];
                            $scope.uploadImage(selectedImage);
                        });

                        setTimeout(function () {
                            $scope.$apply();
                        }, 10);

                        
                    });
                    
                }
            },
            function (error) {
                console.log('Error: ' + error);
            }, {
                maximumImagesCount: 1,
                outputType: 0,
                width: 600,
                height: 400
            });
        }

        $scope.AddUserImage = function () {
            var permissions = cordova.plugins.permissions;

            if (ft)
                ft.abort();

            $scope.userImage = tempUserImage;

            var list = [
                permissions.READ_EXTERNAL_STORAGE,
                permissions.WRITE_EXTERNAL_STORAGE
            ];


            permissions.checkPermission(list, function (status) {
                if (status.hasPermission) {
                    getImage();
                }
                else {
                    permissions.requestPermissions(list, function (status) {
                        if (!status.hasPermission) {

                        }
                        else {
                            getImage();
                        }
                    }, function () {
                    });
                }
            });
        };

        $scope.uploadImage = function (imageSrc) {
            var options = new FileUploadOptions();
            options.fileKey = "image";
            options.chunkedMode = false;
            options.trustAllHosts = true;
            options.httpMethod = "POST";

            if (ft)
                ft.abort();

            ft = new FileTransfer();

            $scope.userImageElement.loading = true;

            ft.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    var percent = Math.ceil(progressEvent.loaded * 100 / progressEvent.total);
                    $scope.userImageElement.progress = percent;
                    setTimeout(function () {
                        $scope.$apply();
                    }, 10);
                }
            };

            
            ft.upload(imageSrc, encodeURI(hostUrl + "/api/v1/advertisement/image"), function (res) {
                var result = JSON.parse(res.response);
                $scope.userImageElement.loading = false;
                if (result.status == true || result.status == "") {
                    $scope.userImageElement.id = result.data.id;
                    $scope.userImageElement.src = result.data.url;
                    tempUserImage = $scope.userImageElement.src;
                }
                else {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في خدمة رفع الصور .', 'alert', function () { });
                }
                setTimeout(function () {
                    $scope.$apply();
                }, 10);

            }, function (error) {
                $scope.userImageElement.loading = false;
                if (error.code != 4) {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في رفع الصورة .', 'alert', function () { });
                }
                setTimeout(function () {
                    $scope.$apply();
                }, 10);

            }, options);
        }

        $scope.submitUserForm = function (EditForm) {
            $scope.editUserProfileReset = true;
            $scope.submittedEditUserProfile = true;
            $scope.isUserImageEmpty = typeof $scope.userImage == "undefined" || $scope.userImage == null || $scope.userImage == '' || $scope.userImage == ' ' ? true : false;

            //$scope.isCityValid = typeof cityId == "undefined" || cityId == null || cityId == '' || cityId == ' ' ? false : true;
            $scope.isCityValid = true;

            var isAllImagesUploaded = true;

            isAllImagesUploaded = $scope.userImageElement.loading == true ? false : true;

            if (EditForm.$valid && $scope.isUserImageEmpty == false && isAllImagesUploaded == true && $scope.isCityValid == true) {

                var userImage = null;

                if (typeof $scope.userImage == 'undefined' || $scope.userImage == null || $scope.userImage.indexOf('.jpg') == -1) {
                    userImage = $scope.userImage;
                }

                userImage = typeof userImage == 'undefined' || userImage == null || userImage.indexOf('placehold.it') > -1 || userImage.indexOf('http') > -1 ? '' : userImage;

                newUserMobile = $scope.editUserProfileForm.mobile;
                var user = {
                    'name': $scope.editUserProfileForm.username,
                    'username': $scope.editUserProfileForm.username,
                    'city_id': cityId,
                    'email': $scope.editUserProfileForm.email,
                    'phone': oldUserMobile,
                    "lat": $rootScope.lat,
                    "lng": $rootScope.lng,
                    'image': userImage
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
               // app.showIndicator();
                appServices.CallService('editProfile', "POST", "api/v1/profile/update", user, function (res2) {
                    app.hideIndicator();
                    if (res2 != null) {
                        if (res2.lat != null && res2.lng!=null) {
                            $rootScope.latUpdate = res2.lat;
                            $rootScope.lngUpdate = res2.lng;
                        }
                        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        userLoggedIn.username = $scope.editUserProfileForm.username;
                        userLoggedIn.name = $scope.editUserProfileForm.username
                        userLoggedIn.city_id = cityId;
                        userLoggedIn.email = $scope.editUserProfileForm.email;
                        userLoggedIn.phone = $scope.editUserProfileForm.mobile;
                        userLoggedIn.image = res2.image;
                        if (res2.image) {
                            $rootScope.userImage = res2.image
                        }
                        CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));
                        SidePanelService.DrawMenu();

                        if (newUserMobile == oldUserMobile) {
                            language.openFrameworkModal('نجاح', 'تم تعديل بياناتك بنجاح .', 'alert', function () { });
                            helpers.GoToPage('userProfile', null);
                        }
                        else {
                            CookieService.setCookie('oldUserMobile', oldUserMobile);
                            var params = {
                                'phone': oldUserMobile,
                                'new_phone': newUserMobile
                            };
                            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                           // app.showIndicator();
                            appServices.CallService('editProfile', "POST", 'api/v1/activation/resend', params, function (res) {
                                app.hideIndicator();
                                if (res != null && res != 'mobileExistBefore') {
                                    language.openFrameworkModal('نجاح', 'تم إرسال الكود للجوال بنجاح .', 'alert', function () { });
                                    CookieService.setCookie('confirmationMobile', newUserMobile);
                                    OpenActivationCodePopup();
                                }
                                else if (res != null && res == 'mobileExistBefore') {
                                    language.openFrameworkModal('خطأ', 'رقم الجوال موجود من قبل .', 'alert', function () { });
                                    var oldUserMobile = CookieService.getCookie('oldUserMobile');
                                    $scope.editUserProfileForm.mobile = oldUserMobile;
                                    setTimeout(function () {
                                        $scope.$apply();
                                    }, fw7.DelayBeforeScopeApply);
                                }
                            });

                        }
                    }
                });
            }
            else {
                if (isAllImagesUploaded == false) {
                    language.openFrameworkModal('خطأ', 'يوجد صورة في طور الرفع , برجاء الإنتظار قبل تعديل بيانات المستخدم .', 'alert', function () { });
                }
            }
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true;    cordova.plugins.notification.badge.clear();
        };

        window.document.addEventListener('backbutton', function (event) {
            var fw7 = myApp.fw7;

            var currentPage = fw7.views[0].activePage.name;
            if (currentPage == 'editProfile') {
                var mobile = CookieService.getCookie('oldUserMobile');
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                userLoggedIn.phone = mobile;
                $scope.editUserProfileForm.mobile = mobile;
                CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
                app.closeModal();
            }

        });

        $scope.form = {};
        $scope.editUserProfileForm = {};


        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

