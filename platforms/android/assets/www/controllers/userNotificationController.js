﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userNotificationController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loadingNotifications;
    var allNotifications = [];
    var isPull = true;

    function ClearListData() {
        allNotifications = [];
        loading = false;
        CookieService.setCookie('notifications-page-number', 1);
        $scope.notifications = null;
    }

    function LoadNotifications(callBack) {
        CookieService.setCookie('userNotification-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var params = {
            'page': parseInt(CookieService.getCookie('userNotification-page-number')),
            'show': myApp.fw7.PageSize
        };

        allNotifications = [];
        app.attachInfiniteScroll('#divInfiniteUserNotifications');

        if (!isPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            //app.showIndicator();
        }
        appServices.CallService('userNotification', 'GET', "api/v1/notifications?page=" + params.page + '&&pageSize=' + params.show, '', function (result) {
            if (!isPull) {
                app.hideIndicator();
            }

            var response = result.data;

            allNotifications = [];

            if (response && response.length > 0) {
                $$('.page[data-page="userNotification"]').removeClass('noResult');

                if (response.length < myApp.fw7.PageSize) {
                    $scope.notificationsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteUserNotifications');
                }
                else {
                    $scope.notificationsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteUserNotifications');
                }

                angular.forEach(response, function (notification) {
                    if (typeof notification.data.n_type != 'undefined' && notification.data.n_type != null && parseInt(notification.data.n_type) < 2) {
                        notification.isRead = notification.read_at != null ? true : false;
                    }
                    else {
                        notification.isRead = true;
                    }
                    allNotifications.push(notification);
                });

                $scope.notifications = allNotifications;
                $scope.noNotifications = allNotifications.length > 0 ? false : true;
            }
            else {
                $$('.page[data-page="userNotification"]').addClass('noResult');
                allNotifications = [];
                $scope.notifications = [];
                $scope.noNotifications = true;
                $scope.notificationsInfiniteLoader = false;
            }

            callBack(true);

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $(document).ready(function () {
        app.onPageInit('userNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'userNotification') return;
            $rootScope.currentOpeningPage = 'userNotification';

            $$('#divInfiniteUserNotifications').on('ptr:refresh', function (e) {
                isPull = true;

                LoadNotifications(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $$('#divInfiniteUserNotifications').on('infinite', function () {
                if (loadingNotifications) return;
                loadingNotifications = true;

                CookieService.setCookie('userNotification-page-number', parseInt(CookieService.getCookie('userNotification-page-number')) + 1);

                var params = {
                    'page': parseInt(CookieService.getCookie('userNotification-page-number')),
                    'show': myApp.fw7.PageSize
                };

                appServices.CallService('userNotification', 'GET', "api/v1/notifications?page=" + params.page + '&&pageSize=' + params.show, '', function (result) {
                    var response = result.data;

                    if (response && response.length > 0) {
                        loadingNotifications = false;

                        angular.forEach(response, function (notification) {
                            notification.isRead = notification.read_at != null ? true : false;
                            allNotifications.push(notification);
                        });

                        $scope.notifications = allNotifications;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.notificationsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteUserNotifications');
                            return;
                        }
                    }
                    else {
                        $scope.notificationsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteUserNotifications');
                        loadingNotifications = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

        });

        
        app.onPageBeforeAnimation('userNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'userNotification') return;
            $rootScope.currentOpeningPage = 'userNotification';

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageReinit('userNotification', function (page) {
            isPull = false;
            $rootScope.RemoveEditPagesFromHistory();
        });


        app.onPageAfterAnimation('userNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'userNotification') return;
            $rootScope.currentOpeningPage = 'userNotification';
            
            isPull = false;

            app.initPullToRefresh('#divInfiniteUserNotifications');

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            LoadNotifications(function (result) { });

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
            }

            $scope.isLoaded = false;
        });


        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };

        $scope.GotoNotificationSettings = function () {
            helpers.GoToPage('notificationSetting', null);
        };

        $scope.ItemIsLongTouched = function (notification) {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'false') {
                app.confirm('هل أنت متأكد من حذف الإشعار ؟', 'تأكيد الحذف', function () {
                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    //app.showIndicator();

                    appServices.CallService('userNotification', 'POST', 'api/v1/notification/delete?notifyId=' + notification.id, '', function (result) {
                        app.hideIndicator();
                        if (result) {
                            language.openFrameworkModal('نجاح', 'تم حذف الإشعار بنجاح .', 'alert', function () { });
                            $scope.notifications = $scope.notifications.filter(function (item) {
                                return item.id !== notification.id;
                            });
                            allNotifications = $scope.notifications;
                            $scope.notifications = allNotifications;
                            $scope.noNotifications = allNotifications.length > 0 ? false : true;
                            if (allNotifications.length > 0) {
                                $$('.page[data-page="userNotification"]').removeClass('noResult');
                            }
                            else {
                                $$('.page[data-page="userNotification"]').addClass('noResult');
                            }
                        }

                        $scope.$apply();
                    });

                }, function () {

                });
            }
        };

        $scope.GoToAdvertisementDetails = function (notification, advertisement) {
            if (notification.isRead == false) {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                //app.showIndicator();
                appServices.CallService('userNotification', 'POST', "api/v1/markasread?notifyId=" + notification.id, '', function (response) {
                    app.hideIndicator();
                    if (response) {
                        cordova.plugins.notification.badge.decrease(1, function (badge) {});
                        notification.isRead = true;
                        if (typeof notification.data.n_type != 'undefined' && notification.data.n_type != null && parseInt(notification.data.n_type) < 2) {
                            if (notification.data.n_type == '1' || notification.data.n_type == 1) {
                                helpers.GoToPage('chat', { conversation_Id: notification.data.convId, adv_id: advertisement.id });
                            }
                            else {
                                helpers.GoToPage('advertisementsDetails', { advertisement: advertisement });
                            }
                        }
                    }
                });
            }
            else {
                if (typeof notification.data.n_type != 'undefined' && notification.data.n_type != null && parseInt(notification.data.n_type) < 2) {
                    if (notification.data.n_type == '1' || notification.data.n_type == 1) {
                        helpers.GoToPage('chat', { conversation_Id: notification.data.convId, adv_id: advertisement.id });
                    }
                    else {
                        helpers.GoToPage('advertisementsDetails', { advertisement: advertisement });
                    }
                }
            }
        };

        app.init();
    });

}]);

