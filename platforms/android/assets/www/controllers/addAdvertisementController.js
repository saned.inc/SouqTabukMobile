﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('addAdvertisementController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var uploadedImages = [];
    var ftMainImage;
    var ftSubImages = [];
    var showMobile = 1;
    var addImage = false;
    var orderImage = 0;

    function LoadCities(callBack) {
        var cities = [];

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        //app.showIndicator();

        $('#linkAddAdvertisementCity .item-after').html('المدينة');
        $scope.addAdvertisementForm.city = null;

        appServices.CallService('addAdvertisement', "GET", "api/v1/cities", '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {
                $scope.cities = res;
                myApp.fw7.Cities = res;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadAdvertisementDetails(advertisement) {

        $scope.IsCategoryEntered = false;
        $scope.IsCityEntered = false;

        $$('#linkAddAdvertisementCategory select').html('');
        $$('#linkAddAdvertisementCity select').html('');

        angular.forEach(myApp.fw7.Categories, function (category) {
            if (category.id == advertisement.category_id) {
                app.smartSelectAddOption('#linkAddAdvertisementCategory select', '<option value="' + category.id + '" selected>' + category.name + '</option>');
                $('#linkAddAdvertisementCategory .item-after').text(category.name);
                $scope.IsCategoryEntered = true;
            }
            else {
                app.smartSelectAddOption('#linkAddAdvertisementCategory select', '<option value="' + category.id + '">' + category.name + '</option>');
            }
        });

        angular.forEach(myApp.fw7.Cities, function (city) {
            if (city.id == advertisement.city_id) {
                app.smartSelectAddOption('#linkAddAdvertisementCity select', '<option value="' + city.id + '" selected>' + city.name + '</option>');
                $('#linkAddAdvertisementCity .item-after').text(city.name);
                $scope.IsCityEntered = true;
            }
            else {
                app.smartSelectAddOption('#linkAddAdvertisementCity select', '<option value="' + city.id + '">' + city.name + '</option>');
            }
        });

        $('#linkAddAdvertisementCategory select').val(advertisement.category_id);
        $('#linkAddAdvertisementCity select').val(advertisement.city_id);

        $scope.addAdvertisementForm.title = advertisement.name;
        $scope.addAdvertisementForm.description = advertisement.description;

        var allAdvertisementImages = [];

        angular.forEach(advertisement.images, function (image,index) {
            var imageToLoad = {
                id: image.id,
                loading: false,
                src: image.url,
                baseString: image.url,
                progress: 0,
                newImage: false,
                index: index
            };

            uploadedImages.push(imageToLoad.id);

            allAdvertisementImages.push(imageToLoad);
        });
        allAdvertisementImages.length > 0 ? $scope.currentIndex = allAdvertisementImages.length : $scope.currentIndex = 0;
        addImage = true;
        if (allAdvertisementImages.length < 6) {
            var indexToFinishAt = 6 - allAdvertisementImages.length;
            var imageIndex = 6 - indexToFinishAt;
            for (var i = 0; i < indexToFinishAt; i++) {
                var imageToLoad = { id: 0, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: imageIndex };
                imageIndex++;
                allAdvertisementImages.push(imageToLoad);
            }
        }
       
        $scope.advertisementMainImage = { id: 1, loading: false, src: advertisement.image, baseString: advertisement.image, progress: 0, newImage: false }
        $scope.advertisementImages = allAdvertisementImages;

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $('#addAdvertisementNotificationsLoaded').hide();
    $('#addAdvertisementNotificationsNotLoaded').show();
    $scope.currentIndex = 0;
    $(document).ready(function () {
        app.onPageInit('addAdvertisement', function (page) {
            if ($rootScope.currentOpeningPage != 'addAdvertisement') return;
            $rootScope.currentOpeningPage = 'addAdvertisement';
            var fromPage = page.fromPage.name;
            if (fromPage != 'map') {
                $scope.resetForm();
            }
        
        });

        app.onPageBack('addAdvertisement', function (page) {
            $scope.resetForm();
            page.query.advertisement = null;
         
          

        });

        app.onPageBeforeAnimation('addAdvertisement', function (page) {
            if ($rootScope.currentOpeningPage != 'addAdvertisement') return;
            $rootScope.currentOpeningPage = 'addAdvertisement';
            $('#showMobile').prop("checked", true);
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            userLoggedIn.phone != null && userLoggedIn.phone != undefined ? $scope.showPhone = true : $scope.showPhone = false;
            $scope.currentIndex = 0;
            orderImage = 0;
            showMobile = 1;
            addImage = false;
           
            var advertisement = page.query.advertisement;
            var fromPage = page.fromPage.name;
            var isEdit=page.query.isEdit;

            if (isEdit == false) {
                page.query.advertisement = null;
                advertisement = null;
            }
            if (fromPage != 'map') {
                $scope.resetForm();
                $rootScope.getLocation();
            }
            if (myApp.fw7.Categories.length == 0) {
                app.smartSelectAddOption('#linkAddAdvertisementCategory select', '<option value="">التصنيف</option>');

                $('#linkAddAdvertisementCategory .item-after').text('التصنيف');
                $$('#linkAddAdvertisementCategory select').val('');
            }

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            LoadCities(function (result) {
                $$('#linkAddAdvertisementCity select').html('');

                angular.forEach(myApp.fw7.Cities, function (city, index) {
                    app.smartSelectAddOption('#linkAddAdvertisementCity select', '<option value="' + city.id + '">' + city.name + '</option>');

                    if (index == 0) {
                        $('#linkAddAdvertisementCity .item-after').text(city.name);
                        $$('#linkAddAdvertisementCity select').val(city.id);
                    }
                });

                $scope.categories = myApp.fw7.Categories;

                var IsVisitor = $rootScope.isVisitor();
                if (IsVisitor == true) {
                    $scope.IsVisitor = true;
                    $('.spanNotifications').hide();
                }
                else {
                    $scope.IsVisitor = false;
                    $rootScope.CheckNotifications(function (notificationLength) {
                        $('#addAdvertisementNotificationsLoaded').show();
                        $('#addAdvertisementNotificationsNotLoaded').hide();
                    });
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

                if (typeof advertisement != 'undefined' && advertisement != null && (fromPage == 'advertisementsDetails' || fromPage=='map')) {
                    $scope.headerText = 'تعديل الإعلان';
                    $scope.buttonText = 'تعديل';
                    CookieService.setCookie('advertisementToEdit', JSON.stringify(advertisement));
                    if (fromPage != 'map') {
                        $rootScope.lat = advertisement.lat;
                        $rootScope.lng = advertisement.lng;
                        nativegeocoder.reverseGeocode(success, failure, $rootScope.lat, $rootScope.lng, { defaultLocale: "en" });
                        function success(result) {
                            if (result[0]) {
                                var res = [];
                                res.push(result[0].countryName);
                                res.push(result[0].administrativeArea);
                                res.push(result[0].locality);
                                res.push(result[0].thoroughfare);
                                res = res.reverse();
                                $rootScope.address = res.toString();
                                setTimeout(function () {
                                    $rootScope.$apply();
                                }, 500)
                            }
                        }
                        function failure(err) {
                            //callback(null)
                            //$rootScope.f7.router.back();
                        }
                        $scope.advertisement = advertisement;
                        LoadAdvertisementDetails(advertisement);
                    }

                   
                }
                else {
                    $scope.headerText = 'إضافة إعلان';
                    $scope.buttonText = 'إضافة';
                    $scope.advertisement = null;
                    $$('#linkAddAdvertisementCategory select').html('');
                    $scope.addAdvertisementForm.category = null;

                    angular.forEach(myApp.fw7.Categories, function (category, index) {
                        app.smartSelectAddOption('#linkAddAdvertisementCategory select', '<option value="' + category.id + '">' + category.name + '</option>');

                        if (index == 0) {
                            $('#linkAddAdvertisementCategory .item-after').text(category.name);
                            $$('#linkAddAdvertisementCategory select').val(category.id);
                        }
                    });
                }
            });

        });

        app.onPageReinit('addAdvertisement', function (page) {

            $scope.isAdvertisementChecked = false;

            $("#chkAdvertisement").prop("checked", $scope.isAdvertisementChecked);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('addAdvertisement', function (page) {
            if ($rootScope.currentOpeningPage != 'addAdvertisement') return;
            $rootScope.currentOpeningPage = 'addAdvertisement';
            //$('#addDisableImage0').addClass("disabledImage");
        });

        function ConvertImgToBase64Url(url, outputFormat, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                var dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL, url);
                canvas = null;
            };

            img.src = url;
        }

        $('#showMobile').on('change', function () {
            var checked = $(this).prop('checked');
            checked==true ? showMobile = 1 : showMobile = 0
        });

        $scope.uploadMainImage = function (image) {
            var options = new FileUploadOptions();
            options.fileKey = "image";
            options.chunkedMode = false;
            options.trustAllHosts = true;
            options.httpMethod = "POST";

            if (ftMainImage)
                ftMainImage.abort();

            ftMainImage = new FileTransfer();

            $scope.advertisementMainImage.loading = true;

            ftMainImage.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    var percent = Math.ceil(progressEvent.loaded * 100 / progressEvent.total);
                    $scope.advertisementMainImage.progress = percent;
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
            };

            ftMainImage.upload(image.src, encodeURI(serviceURL + "/api/v1/advertisement/image?is_main=1"), function (res) {
                var result = JSON.parse(res.response);
                $scope.advertisementMainImage.loading = false;
                $scope.advertisementMainImage.newImage = false;
                if (result.status == true || result.status == "") {
                    $scope.advertisementMainImage.id = 11111;
                    $scope.advertisementMainImage.src = result.data.image;
                    $scope.advertisementMainImageFile = result.data.image;
                    $scope.advertisementMainImageFileThumb = result.data.thumb;
                }
                else {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في خدمة رفع الصور .', 'alert', function () { });
                }
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }, function (error) {
                $scope.advertisementMainImage = { id: 0, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false }
                if (error.code != 4) {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في رفع الصورة .', 'alert', function () { });
                }
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }, options);
        }

        function getImage(image, index) {
            $scope.getPermession(function () {

                window.imagePicker.getPictures(function (results) {
                    if (results && results.length > 0) {
                        addImage = true
                        if (image.src == "img/image.png") {
                            $scope.currentIndex += 1;
                        }
                        var isImageBinded = false;

                        angular.forEach(results, function (uploadedImage) {
                            var selectedImage = uploadedImage;

                            angular.forEach($scope.advertisementImages, function (advImage, index) {
                                if (image.index == advImage.index && !isImageBinded) {
                                    isImageBinded = true;
                                    advImage.loading = false;
                                    advImage.src = selectedImage;
                                    advImage.newImage = true;
                                    advImage.baseString = selectedImage;
                                    advImage.progress = 0;
                                    advImage.index = $scope.advertisementImages.findIndex(x => x.id == advImage.id);
                                    advImage.ft = new FileTransfer();
                                    $scope.$apply();

                                    $scope.uploadImage(advImage, orderImage, function (result) { });

                                }
                            });

                        });

                    }
                },
                    function (error) {
                        console.log('Error: ' + error);
                    }, {
                        maximumImagesCount: 1,
                        outputType: 0,
                        width: 1280,
                        height: 800
                    });
            })
           
        }

        function getMainImage() {
            $scope.getPermession(function () {
                window.imagePicker.getPictures(function (results) {
                    if (results && results.length > 0) {

                        angular.forEach(results, function (image) {
                            $scope.advertisementMainImage.loading = false;
                            $scope.advertisementMainImage.src = image;
                            $scope.advertisementMainImage.newImage = true;
                            $scope.advertisementMainImage.baseString = image;
                            $scope.advertisementMainImage.progress = 0;

                            ConvertImgToBase64Url(image, 'jpg', function (imgBase64, image) {
                                $scope.advertisementMainImageBase64 = imgBase64.split(',')[1];
                            });

                            $scope.uploadMainImage($scope.advertisementMainImage);
                        });

                        setTimeout(function () {
                            $scope.$apply();
                        }, 10);

                    }
                },
                    function (error) {
                        console.log('Error: ' + error);
                    }, {
                        maximumImagesCount: 1,
                        outputType: 0,
                        width: 1280,
                        height: 800
                    });
            })
          
        }

        function getMultipleImages() {
            $scope.getPermession(function () {

                window.imagePicker.getPictures(function (results) {
                    if (results && results.length > 0) {

                        $scope.advertisementImages = [
                            { id: 1, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 0 },
                            { id: 2, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 1 },
                            { id: 3, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 2 },
                            { id: 4, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 3 },
                            { id: 5, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 4 },
                            { id: 6, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 5 }
                        ];

                        uploadedImages = [];

                        angular.forEach(results, function (uploadedImage, index) {
                            var selectedImage = uploadedImage;

                            var advImage = $scope.advertisementImages[index];
                            advImage.loading = false;
                            advImage.src = selectedImage;
                            advImage.newImage = true;
                            advImage.baseString = selectedImage;
                            advImage.progress = 0;
                            advImage.index = 10;
                            advImage.ft = new FileTransfer();
                        });

                        setTimeout(function () {
                            $scope.$apply();
                        }, 10);

                        angular.forEach($scope.advertisementImages, function (imageToUpload, index) {
                            if (imageToUpload.newImage == true) {
                                $scope.uploadImage(imageToUpload, function (result) { });
                            }
                        });


                    }
                },
                    function (error) {
                        console.log('Error: ' + error);
                    }, {
                        maximumImagesCount: 6,
                        outputType: 0,
                        width: 1280,
                        height: 800
                    });
            })
           
        }

        $scope.AddAdvertisementMainImage = function () {
            var permissions = cordova.plugins.permissions;

            var list = [
                permissions.READ_EXTERNAL_STORAGE,
                permissions.WRITE_EXTERNAL_STORAGE
            ];
            function error() {
                console.warn('Storage permissions is not turned on');
            }

            permissions.checkPermission(list, function (status) {
                if (status.hasPermission) {
                    getMainImage();
                }
                else {
                    permissions.requestPermissions(list, function (status) {
                        if (!status.hasPermission) error();
                        getMainImage();
                    }, error);
                }
            });
        };

        $scope.AddAdvertisementImage = function (image, index) {
            if (!addImage && index != 0)
                return;
            if (index > $scope.currentIndex) {
                return;
            }
            else if (index==0) {
                orderImage=0
            }

            var permissions = cordova.plugins.permissions;

            var list = [
                permissions.READ_EXTERNAL_STORAGE,
                permissions.WRITE_EXTERNAL_STORAGE
            ];

            function error() {
                console.warn('Storage permissions is not turned on');
            }

            permissions.checkPermission(list, function (status) {
                if (status.hasPermission) {
                    getImage(image,index);
                }
                else {
                    permissions.requestPermissions(list, function (status) {
                        if (!status.hasPermission) error();
                        getImage(image,index);
                    }, error);
                }
            });
        };

        $scope.getPermession = function (callback) {
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
                if (status.hasPermission) {
                    callback();
                } else {
                    permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
                        if (status.hasPermission == true)
                            callback();
                    }, function (error) {
                        console.log("sss");
                    });
                }
            });
        }

        $scope.uploadImage = function (image,index,callBack) {
            var options = new FileUploadOptions();
            options.fileKey = "image";
            options.chunkedMode = false;
            options.trustAllHosts = true;
            options.httpMethod = "POST";
            options.order=index

            if (image.ft)
                image.ft.abort();

            image.ft = new FileTransfer();

            image.loading = true;

            image.ft.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    var percent = Math.ceil(progressEvent.loaded * 100 / progressEvent.total);
                    image.progress = percent;
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
            };

            image.ft.upload(image.src, encodeURI(serviceURL + "/api/v1/advertisement/image?is_main=0&order="+index), function (res) {
                var result = JSON.parse(res.response);
                image.loading = false;
                image.newImage = false;
                var imageOrder = {

                }
                if (result.status == true || result.status == "") {
                    image.id = result.data.id;
                    image.src = result.data.url;
                    uploadedImages.splice(image.index, 1);
                    uploadedImages.push(result.data.id);
                    console.log(image.src);
                    console.log(JSON.stringify(uploadedImages));
                    orderImage+=1;
                }
                else {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في خدمة رفع الصور الفرعية .', 'alert', function () { });
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

                callBack(true);

            }, function (error) {
                image.loading = false;
                image.newImage = false;

                angular.forEach($scope.advertisementImages, function (advImage) {
                    if (image.index == advImage.index) {
                        advImage.id = image.id;
                        advImage.loading = false;
                        advImage.src = 'img/image.png';
                        advImage.newImage = false;
                        advImage.baseString = 'img/image.png';
                        advImage.progress = 0;
                        advImage.index = image.index;
                        advImage.ft = new FileTransfer();
                        $scope.$apply();
                    }
                });

                if (error.code != 4) {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في رفع أحد الصور الفرعية .', 'alert', function () { });
                }
                
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
                callBack(false);
            }, options);
        }

        $scope.UploadFinalImages = function () {
            angular.forEach($scope.advertisementImages, function (image) {
                if (image.ft)
                    image.ft.abort();
            });


            var permissions = cordova.plugins.permissions;

            var list = [
                permissions.READ_EXTERNAL_STORAGE,
                permissions.WRITE_EXTERNAL_STORAGE
            ];

            function error() {
                console.warn('Storage permissions is not turned on');
            }

            permissions.checkPermission(list, function (status) {
                if (status.hasPermission) {
                    getMultipleImages();
                }
                else {
                    permissions.requestPermissions(list, function (status) {
                        if (!status.hasPermission) error();
                        getMultipleImages();
                    }, error);
                }
            });

        };

        $scope.resetForm = function () {
            $scope.submittedaddAdvertisement = false;
            $scope.addAdvertisementReset = false;
            $scope.addAdvertisementForm.title = null;
            $rootScope.lat = null
            $rootScope.lng = null
            $rootScope.latUpdate = null
            $rootScope.lngUpdate = null
            $rootScope.address = null;
            
            $scope.addAdvertisementForm.description = null;
            $scope.addAdvertisementForm.city = null;
            $scope.addAdvertisementForm.category = null;
            $scope.IsCategoryEntered = false;
            $scope.advertisement = {};
            $scope.isAdvertisementChecked = false;

            $scope.advertisementMainImage = { id: 0, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false }

            $scope.advertisementImages = [
                { id: 1, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 0 },
                { id: 2, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 1 },
                { id: 3, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 2 },
                { id: 4, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 3 },
                { id: 5, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 4 },
                { id: 6, loading: false, src: 'img/image.png', baseString: 'img/image.png', progress: 0, newImage: false, index: 5 }
            ];

            uploadedImages = [];

            if (ftMainImage)
                ftMainImage.abort();

            angular.forEach($scope.advertisementImages, function (image) {
                if (image.ft)
                    image.ft.abort();
            });
            

            if (typeof $scope.AddAdvertisementForm != 'undefined' && $scope.AddAdvertisementForm != null) {
                $scope.AddAdvertisementForm.$setPristine(true);
                $scope.AddAdvertisementForm.$setUntouched();
            }

            $("#chkAdvertisement").prop("checked", $scope.isAdvertisementChecked);
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitAddAdvertisementForm = function (submittedForm) {
           
            var isAllImagesUploaded = true;
            var atLeastOneImageAdded = false;

            isAllImagesUploaded = $scope.advertisementMainImage.loading == true ? false : true;

            angular.forEach($scope.advertisementImages, function (imageToUpload, index) {
                if (imageToUpload.loading == true) {
                    isAllImagesUploaded = false;
                }
            });

            uploadedImages = uploadedImages.filter(function (obj) {
                return obj > 0;
            });

            atLeastOneImageAdded = $scope.advertisementMainImage.src == 'img/image.png' || (typeof uploadedImages != 'undefined' && uploadedImages != null && uploadedImages.length == 0) ? false : true;

            //if (atLeastOneImageAdded == true) {
                if (isAllImagesUploaded == true) {
                    //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    //app.showIndicator();

                    $scope.addAdvertisementReset = true;
                    $scope.submittedaddAdvertisement = true;
                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                    var methodName = typeof $scope.advertisement != 'undefined' && $scope.advertisement != null ? 'api/v1/advertisement/update' : 'api/v1/advertisement/create';
                    var advertisementId = typeof $scope.advertisement != 'undefined' && $scope.advertisement != null ? $scope.advertisement.id : 0;
                    var successMessage = typeof $scope.advertisement != 'undefined' && $scope.advertisement != null ? 'تم تعديل الإعلان بنجاح , شكرا لك .' : 'تم إضافة الإعلان بنجاح , شكرا لك .';
                    var isEdit = typeof $scope.advertisement != 'undefined' && $scope.advertisement != null ? true : false;

                    uploadedImages = uploadedImages.filter(function (obj) {
                        return obj > 0;
                    });

                    $scope.filteredAdvertisementImages = $scope.advertisementImages.filter(function (obj) {
                        return obj.src != 'img/image.png';
                    });

                    var uploadedImagesIDs = typeof uploadedImages != 'undefined' && uploadedImages != null && uploadedImages.length > 0 ? uploadedImages : $scope.filteredAdvertisementImages.map(a => a.id);

                    //$scope.advertisementMainImageBase64 = typeof $scope.advertisementMainImageBase64 != 'undefined' && $scope.advertisementMainImageBase64 != null ? $scope.advertisementMainImageBase64 : $scope.advertisementMainImage.src;
                    

                    //$scope.advertisementMainImageBase64 = $scope.advertisementMainImageBase64 == 'img/image.png' ? null : $scope.advertisementMainImageBase64;
                    //$scope.advertisementMainImageFile = $scope.advertisementMainImageFile == 'img/image.png' ? null : $scope.advertisementMainImageFile;

                    $scope.advertisementMainImageFile = $scope.advertisementMainImageFile == 'img/image.png' || typeof $scope.advertisementMainImageFile == 'undefined' ? null : $scope.advertisementMainImageFile;
                    $scope.advertisementMainImageFileThumb = $scope.advertisementMainImageFileThumb == 'img/image.png' || typeof $scope.advertisementMainImageFileThumb == 'undefined' ? null : $scope.advertisementMainImageFileThumb;

                    var categoryId = $('#linkAddAdvertisementCategory select').val();
                    var cityId = $('#linkAddAdvertisementCity select').val();
                    $scope.IsCategoryEntered = categoryId != null && categoryId != '' && categoryId != ' ' ? true : false;
                    $scope.IsCityEntered = cityId != null ? true : false;

                    if (submittedForm.$valid && $scope.IsCategoryEntered == true && $scope.IsCityEntered == true) {
                        if (uploadedImages.length<=0) {
                            language.openFrameworkModal('خطأ', 'يجب رفع صور للإعلان أولا .', 'alert', function () { });
                            return
                        }
                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                        var params = {
                            'name': $scope.addAdvertisementForm.title,
                            'description': $scope.addAdvertisementForm.description,
                            'category': categoryId,
                            'city': cityId,
                            'imagesIds': uploadedImagesIDs.toString(),
                            'lat': $rootScope.lat,
                            'lng': $rootScope.lng,
                            'thumb': $scope.advertisementMainImageFileThumb,
                            'show_phone': showMobile
                        };

                        if (methodName == 'api/v1/advertisement/update') {
                            params.advId = advertisementId;
                        }


                        appServices.CallService('addAdvertisement', "POST", methodName, params, function (res2) {
                            app.hideIndicator();
                            if (res2 != null) {
                                language.openFrameworkModal('نجاح', successMessage, 'alert', function () {
                                    $scope.resetForm();

                                    var advertisementToEdit = JSON.parse(CookieService.getCookie('advertisementToEdit'));

                                    if (isEdit) {
                                        //helpers.GoToPage('advertisementsDetails', { advertisement: advertisementToEdit });
                                        helpers.GoToPage('advertisements', { methodName: 'api/v1/myadvs', children: 100 });
                                    }
                                    else {

                                        var categoriesStored = myApp.fw7.Categories;

                                        helpers.GoToPage('advertisements', { methodName: 'api/v1/advertisement/category/list', categoryId: categoryId, children: res2.children });
                                    }
                                });
                            }
                        });
                    }
                    else {
                        app.hideIndicator();
                    }
                }
                else {
                    language.openFrameworkModal('خطأ', 'يوجد صورة أو أكثر في طور الرفع , برجاء الإنتظار قبل إضافة الإعلان .', 'alert', function () { });
                }
            //}
            //else {
            //    language.openFrameworkModal('خطأ', 'يجب رفع صور للإعلان أولا .', 'alert', function () { });
            //}
        };

        $scope.form = {};
        $scope.addAdvertisementForm = {};

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        $("#lblAdvertisement").on("click", function () {
            var isTermsChecked = $('#chkAdvertisement').prop("checked");
            $scope.isAdvertisementChecked = !isTermsChecked;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        $scope.GoBack = function () {
            helpers.GoBack();
        };
        $rootScope.openMap = function () {
            helpers.GoToPage('map', null);
        }

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null);
            $rootScope.notifyClick = true;
            cordova.plugins.notification.badge.clear();
        };

        $scope.GoToTerms = function () {
            $scope.isAdvertisementChecked = false;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
            helpers.GoToPage('terms', null);
        };

        window.document.addEventListener('backbutton', function (event) {
            if ($rootScope.currentOpeningPage == 'addAdvertisement') {
                if (ftMainImage)
                    ftMainImage.abort();

                angular.forEach($scope.advertisementImages, function (image) {
                    if (image.ft)
                        image.ft.abort();
                });
            }

        });

        app.init();
    });

}]);

