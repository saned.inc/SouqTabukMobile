﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('advertisementsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingAdvertisements;
    var allAdvertisements = [];
    var isPull = true;
    var methodName;
    var advCount;
    var myAdvsCategoryId;
    var searchInfinite = false;
    var categoryId;
    var adTitle;
    var catId;

    function ClearListData() {
        allAdvertisements = [];
        loadingAdvertisements = false;
        CookieService.setCookie('advertisements-page-number', 1);
        $scope.advertisements = null;
        app.pullToRefreshDone();
    }

    function CreateThumbnail(image,callBack) {
        var canvas = document.createElement("canvas");
        canvas.width = 360;
        canvas.height = 230;
        var c = canvas.getContext("2d");
        var img = new Image();
        
        img.onload = function (e) {
            c.drawImage(this, 0, 0, 400, 200);
            var thumbURL = canvas.toDataURL("image/jpg");
            callBack(thumbURL.split(',')[1]);
        };

        img.src = image;
    }

    function OpenLongTouchPopup(pageName, advertisementId) {
        var afterTextMessage = pageName == 'MyAdv' ? '<p>هل أنت متأكد من حذف الإعلان ؟</p>' : '<p>هل أنت متأكد من حذف الإعلان من قائمة المفضلة؟</p>';
        var isMyAdv = pageName == 'MyAdv' ? true : false;
        var longTouchPopup = app.modal({
            title: 'تأكيد',
            text: '',
            afterText: afterTextMessage,
            buttons: [
                {
                    text: 'حذف',
                    onClick: function () {
                        if (isMyAdv == false) {
                            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                          //  app.showIndicator();
                            appServices.CallService('advertisements', 'POST', 'api/v1/unfavorite?advId=' + advertisementId, '', function (result) {
                                app.hideIndicator();
                                if (result) {
                                    $scope.advertisements = $scope.advertisements.filter(function (item) {
                                        return item.id !== advertisementId;
                                    });
                                    allAdvertisements = $scope.advertisements;
                                    $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                                    if (allAdvertisements.length > 0) {
                                        $('.advNoResult').hide();
                                    }
                                    else {
                                        $('.advNoResult').show();
                                    }

                                    setTimeout(function () {
                                        $scope.$apply();
                                    }, fw7.DelayBeforeScopeApply);
                                }
                                else {
                                    app.closeModal(longTouchPopup);
                                }

                                $scope.shareFavoriteLoad = false;
                                $scope.$apply();
                            });
                        }
                        else {
                            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                           // app.showIndicator();
                            appServices.CallService('advertisements', 'POST', 'api/v1/advertisement/delete?advId=' + advertisementId, '', function (result) {
                                app.hideIndicator();
                                if (result) {
                                    $scope.advertisements = $scope.advertisements.filter(function (item) {
                                        return item.id !== advertisementId;
                                    });
                                    allAdvertisements = $scope.advertisements;
                                    $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                                    if (allAdvertisements.length > 0) {
                                        $('.advNoResult').hide();
                                    }
                                    else {
                                        $('.advNoResult').show();
                                    }

                                    var categoriesStored = myApp.fw7.Categories;

                                    angular.forEach(categoriesStored, function (filteredCategory) {
                                        if (filteredCategory.id == myAdvsCategoryId) {
                                            filteredCategory.children = parseInt(filteredCategory.children) - 1;
                                        }
                                    });

                                    setTimeout(function () {
                                        $scope.$apply();
                                    }, fw7.DelayBeforeScopeApply);
                                }
                                else {
                                    app.closeModal(longTouchPopup);
                                }

                                $scope.shareFavoriteLoad = false;
                                $scope.$apply();
                            });
                        }
                    }
                },
                {
                    text: 'إلغاء',
                    onClick: function () {
                        app.closeModal(longTouchPopup);
                    }
                }
            ]
        })
    }

    function LoadEmptyTemplates() {
        var advertisements = [];
        $scope.allAdvertisementsLoaded = false;

        for (var i = 0; i < advCount; i++) {
            advertisements.push({ id: i, image: 'img/puff.svg', index: i });
        }

        $scope.advertisements = advertisements;        
    }

    function LoadAdvertisements(categoryId, methodName, callBack) {
        $scope.noAdvertisements = false;
        $('#advNoResult').hide();
        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);

        if (advCount != 'undefined' && advCount != null && parseInt(advCount) > 0) {
            LoadEmptyTemplates();
        }
        else {

            allAdvertisements = [];
            $scope.advertisements = [];
            //$scope.noAdvertisements = true;
            //$('.advNoResult').show();
            $scope.advertisementsInfiniteLoader = false;

            callBack(true);

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        CookieService.setCookie('advertisements-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        if (methodName == 'api/v1/advertisement/category/list') {
            methodName = methodName + '?categoryId=' + categoryId + '&&page=' + parseInt(CookieService.getCookie('advertisements-page-number')) + '&&page_size=' + 10;
        }
        else {
            methodName = methodName + '?page=' + parseInt(CookieService.getCookie('advertisements-page-number')) + '&&page_size=' +10
        }

        allAdvertisements = [];
        app.attachInfiniteScroll('#divInfiniteAdvertisements');

        appServices.CallService('advertisements', 'GET', methodName, '', function (result) {
            var response = result && result.data ? result.data : null;

            allAdvertisements = [];

            if (response && response.length > 0) {
                if (response.length < 10) {
                    $scope.advertisementsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteAdvertisements');
                }
                else {
                    $scope.advertisementsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteAdvertisements');
                }

                myAdvsCategoryId = response[0].category_id;

                var thumbIndex = 0;
                var advertisementLength=response.length;

                angular.forEach(response, function (advertisement) {
                    advertisement.day = advertisement.created_at.split(' ')[0];
                    allAdvertisements.push(advertisement);
                });

                allAdvertisements.sort(function (a, b) {
                    return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
                });

                angular.forEach($scope.advertisements, function (advertisement, index) {
                    var adv = allAdvertisements[index];
                    if (adv && adv.id) {
                        advertisement.id = adv.id;
                        advertisement.image = adv.image;
                        advertisement.name = adv.name;
                        advertisement.city = adv.city;
                        advertisement.day = adv.day;
                        advertisement.index = adv.index;
                        advertisement.user = adv.user;
                        advertisement.rating = adv.rating;
                        advertisement.is_sold = adv.is_sold;
                        advertisement.isFavorite = adv.isFavorite;
                        advertisement.isFollowed = adv.isFollowed;
                    }
                });
                $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                $('.advNoResult').hide();
                $scope.advertisements = allAdvertisements;
                $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                if (isPull) {
                    app.pullToRefreshDone();
                }
              
                callBack(true);

                setTimeout(function () {
                    $scope.allAdvertisementsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                $scope.allAdvertisementsLoaded = true;
                allAdvertisements = [];
                $scope.advertisements = [];
                $scope.noAdvertisements = true;
                $('.advNoResult').show();
                $scope.advertisementsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $('#advertisementsNotificationsLoaded').hide();
    $('#advertisementsNotificationsNotLoaded').show();
           
    $(document).ready(function () {
        $$('#divInfiniteAdvertisements').on('ptr:refresh', function (e) {
            if (searchInfinite) {
                //app.showIndicator();
                appServices.CallService('activation', "GET", "api/v1/advertisement/search?categoryId=" + categoryId + "&adName=" + adTitle + "&pageSize=200&page=1", "", function (response) {
                    app.hideIndicator();
                    app.pullToRefreshDone();
                    if (response != null && response.data.length > 0) {
                        if (response.length < 200) {
                            $scope.advertisementsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteAdvertisements');
                        }
                        else {
                            $scope.advertisementsInfiniteLoader = true;
                            app.attachInfiniteScroll('#divInfiniteAdvertisements');
                        }

                        myAdvsCategoryId = response[0].category_id;

                        var thumbIndex = 0;
                        var advertisementLength = response.length;

                        angular.forEach(response, function (advertisement) {
                            advertisement.day = advertisement.created_at.split(' ')[0];
                            allAdvertisements.push(advertisement);
                        });

                        allAdvertisements.sort(function (a, b) {
                            return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
                        });

                        angular.forEach($scope.advertisements, function (advertisement, index) {
                            var adv = allAdvertisements[index];
                            if (adv && adv.id) {
                                advertisement.id = adv.id;
                                advertisement.image = adv.image;
                                advertisement.name = adv.name;
                                advertisement.city = adv.city;
                                advertisement.day = adv.day;
                                advertisement.index = adv.index;
                                advertisement.user = adv.user;
                                advertisement.rating = adv.rating;
                                advertisement.is_sold = adv.is_sold;
                                advertisement.isFavorite = adv.isFavorite;
                                advertisement.isFollowed = adv.isFollowed;
                            }
                        });
                        $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                        $('.advNoResult').hide();
                        $scope.advertisements = allAdvertisements;
                        $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                        if (isPull) {
                            app.pullToRefreshDone();
                        }

                        setTimeout(function () {
                            $scope.allAdvertisementsLoaded = true;
                            $scope.isLoaded = true;
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    }
                    else {
                        language.openFrameworkModal('نجاح', 'لايوجد اعلانات', 'alert', function () { });
                    }
                });
            }
            else {
                isPull = true;
                var methodNameStored = CookieService.getCookie('methodName');
                var categoryIdStored = CookieService.getCookie('categoryId');
                var storedChildren = CookieService.getCookie('storedChildren');

                if (methodNameStored != 'api/v1/advertisement/category/list') {
                    appServices.CallService('userProfile', 'GET', "api/v1/profile", '', function (userData) {
                        if (userData) {
                            CookieService.setCookie('userLoggedIn', JSON.stringify(userData));
                            $scope.allAdvertisementsLoaded = false;

                            if (methodNameStored == 'api/v1/myadvs') {
                                advCount = userData.advertisementCount;
                            }
                            else {
                                advCount = userData.favoritesCount;
                            }

                            allAdvertisements = [];

                            LoadAdvertisements(categoryIdStored, methodNameStored, function (result) {
                                app.pullToRefreshDone();
                            });

                            var IsVisitor = $rootScope.isVisitor();
                            if (IsVisitor == true) {
                                $scope.IsVisitor = true;
                                $('.spanNotifications').hide();
                            }
                            else {
                                $scope.IsVisitor = false;
                                $rootScope.CheckNotifications(function (notificationLength) {
                                    $('#advertisementsNotificationsLoaded').show();
                                    $('#advertisementsNotificationsNotLoaded').hide();
                                });
                            }

                            setTimeout(function () {
                                $scope.$apply();
                            }, fw7.DelayBeforeScopeApply);
                        }
                    });
                }
                else {
                    advCount = storedChildren;

                    LoadAdvertisements(categoryIdStored, methodNameStored, function (result) {
                        app.pullToRefreshDone();
                    });

                    var IsVisitor = $rootScope.isVisitor();
                    if (IsVisitor == true) {
                        $scope.IsVisitor = true;
                        $('.spanNotifications').hide();
                    }
                    else {
                        $scope.IsVisitor = false;
                        $rootScope.CheckNotifications(function (notificationLength) {
                            $('#advertisementsNotificationsLoaded').show();
                            $('#advertisementsNotificationsNotLoaded').hide();
                        });
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
            }
         
        });

        app.onPageInit('advertisements', function (page) {
            if ($rootScope.currentOpeningPage != 'advertisements') return;
            $rootScope.currentOpeningPage = 'advertisements';


            $$('#divInfiniteAdvertisements').on('infinite', function () {
                if (loadingAdvertisements) return;
                loadingAdvertisements = true;
                if (searchInfinite) {
                    return
                }
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                var methodNameStored = CookieService.getCookie('methodName');
                var categoryIdStored = CookieService.getCookie('categoryId');
               
                CookieService.setCookie('advertisements-page-number', parseInt(CookieService.getCookie('advertisements-page-number')) + 1);

                if (methodNameStored == 'api/v1/advertisement/category/list') {
                    methodNameStored = methodNameStored + '?categoryId=' + catId + '&&page=' + parseInt(CookieService.getCookie('advertisements-page-number')) + '&&page_size=' + 10;
                }
                else {
                    methodNameStored = methodNameStored + '?page=' + parseInt(CookieService.getCookie('advertisements-page-number')) + '&&page_size=' +10
                }

                appServices.CallService('advertisements', 'GET', methodNameStored, '', function (result) {
                    var response = result.data;

                    if (response && response.length > 0) {
                        loadingAdvertisements = false;

                        angular.forEach(response, function (advertisement) {
                            advertisement.day = advertisement.created_at.split(' ')[0];
                            allAdvertisements.push(advertisement);
                        });

                        $scope.advertisements = allAdvertisements;

                        if (response && response.length < 10) {
                            $scope.advertisementsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteAdvertisements');
                            return;
                        }
                    }
                    else {
                        $scope.advertisementsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteAdvertisements');
                        loadingAdvertisements = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('advertisements', function (page) {
            if ($rootScope.currentOpeningPage != 'advertisements') return;
            $rootScope.currentOpeningPage = 'advertisements';
            searchInfinite = false;
            if ($rootScope.sponsors) {
                $rootScope.initalHomeSwiper('#adSwiper', $rootScope.sponsors)
            }
            if (page.fromPage.name == "advancedSearch") {
                searchInfinite = true;
                $scope.allAdvertisementsLoaded = false;
                advCount = 10;
                LoadEmptyTemplates();
                ClearListData();
                var response = page.query.searchResults
                var categoryId = page.query.categoryId
                var adTitle = page.query.adTitle
                if (response.length < 200) {
                    $scope.advertisementsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteAdvertisements');
                }
                else {
                    $scope.advertisementsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteAdvertisements');
                }

                myAdvsCategoryId = response[0].category_id;

                var thumbIndex = 0;
                var advertisementLength = response.length;

                angular.forEach(response, function (advertisement) {
                    advertisement.day = advertisement.created_at.split(' ')[0];
                    allAdvertisements.push(advertisement);
                });

                allAdvertisements.sort(function (a, b) {
                    return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
                });

                angular.forEach($scope.advertisements, function (advertisement, index) {
                    var adv = allAdvertisements[index];
                    if (adv && adv.id) {
                        advertisement.id = adv.id;
                        advertisement.image = adv.image;
                        advertisement.name = adv.name;
                        advertisement.city = adv.city;
                        advertisement.day = adv.day;
                        advertisement.index = adv.index;
                        advertisement.user = adv.user;
                        advertisement.rating = adv.rating;
                        advertisement.is_sold = adv.is_sold;
                        advertisement.isFavorite = adv.isFavorite;
                        advertisement.isFollowed = adv.isFollowed;
                    }
                });
                $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                $('.advNoResult').hide();
                $scope.advertisements = allAdvertisements;
                $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
                if (isPull) {
                    app.pullToRefreshDone();
                }
                var IsVisitor = $rootScope.isVisitor();
                if (IsVisitor == true) {
                    $scope.IsVisitor = true;
                    $('.spanNotifications').hide();
                }
                else {
                    $scope.IsVisitor = false;
                    $rootScope.CheckNotifications(function (notificationLength) {
                        $('#advertisementsNotificationsLoaded').show();
                        $('#advertisementsNotificationsNotLoaded').hide();
                    });
                }

                setTimeout(function () {
                    $scope.allAdvertisementsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }
            else {
                $scope.allAdvertisementsLoaded = false;
                advCount = 10;
                LoadEmptyTemplates();
                var methodName = page.query.methodName;
                categoryId = page.query.categoryId;
                catId = categoryId;
            
                var children;

                //appServices.CallService('home', "GET", 'api/v1/children/counts?categoryId=' + categoryId, '', function (res) {
                    //if (res) {
                        children = 10;
                        $scope.isLoaded = true;

                        ClearListData();

                        $('.advNoResult').hide();

                        var methodName = page.query.methodName;
                           categoryId = page.query.categoryId;
                        var children = page.query.children;

                        CookieService.setCookie('methodName', methodName);
                        CookieService.setCookie('categoryId', categoryId);
                        CookieService.setCookie('storedChildren', children);

                        if (methodName == 'api/v1/advertisement/category/list') {
                            $scope.pageName = 'الإعلانات';
                        }
                        else if (methodName == 'api/v1/myadvs') {
                            $scope.pageName = 'إعلاناتي';
                        }
                        else {
                            $scope.pageName = 'قائمة المفضلة';
                        }

                        isPull = false;

                        // app.initPullToRefresh('#divInfiniteAdvertisements');

                        $rootScope.CheckNewMessages(function (notifications) {
                            $scope.NewNotificationsNumber = notifications;
                            setTimeout(function () {
                                $scope.$apply();
                            }, 10);
                        });

                        ClearListData();

                        if (methodName != 'api/v1/advertisement/category/list') {
                           // SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                            //app.showIndicator();
                            $scope.allAdvertisementsLoaded = false;
                            advCount=10
                            LoadAdvertisements(categoryId, methodName, function (result) { });

                            var IsVisitor = $rootScope.isVisitor();
                            if (IsVisitor == true) {
                                $scope.IsVisitor = true;
                                $('.spanNotifications').hide();
                            }
                            else {
                                $scope.IsVisitor = false;
                                $rootScope.CheckNotifications(function (notificationLength) {
                                    $('#advertisementsNotificationsLoaded').show();
                                    $('#advertisementsNotificationsNotLoaded').hide();
                                });
                            }

                            setTimeout(function () {
                                $scope.$apply();
                            }, fw7.DelayBeforeScopeApply);
                            //appServices.CallService('userProfile', 'GET', "api/v1/profile", '', function (userData) {
                            //    app.hideIndicator();
                              
                            //    if (userData) {
                            //        CookieService.setCookie('userLoggedIn', JSON.stringify(userData));
                            //        $scope.allAdvertisementsLoaded = false;

                            //        if (methodName == 'api/v1/myadvs') {
                            //            advCount = userData.advertisementCount;
                            //        }
                            //        else {
                            //            advCount = userData.favoritesCount;
                            //        }

                                
                            //    }
                            //});
                        }
                        else {
                            advCount = children;

                            LoadAdvertisements(categoryId, methodName, function (result) { });

                            var IsVisitor = $rootScope.isVisitor();
                            if (IsVisitor == true) {
                                $scope.IsVisitor = true;
                                $('.spanNotifications').hide();
                            }
                            else {
                                $scope.IsVisitor = false;
                                $rootScope.CheckNotifications(function (notificationLength) {
                                    $('#advertisementsNotificationsLoaded').show();
                                    $('#advertisementsNotificationsNotLoaded').hide();
                                });
                            }

                            setTimeout(function () {
                                $scope.$apply();
                            }, fw7.DelayBeforeScopeApply);
                        }
                    //}
                    //else {

                    //}
                //});

            }
        });

        app.onPageReinit('advertisements', function (page) {

            isPull = false;

            $('.advNoResult').hide();

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('advertisements', function (page) {
            if ($rootScope.currentOpeningPage != 'advertisements') return;
            $rootScope.currentOpeningPage = 'advertisements';

            
        });

 

        $scope.ItemIsLongTouched = function (advertisement) {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'false') {
                if ($scope.pageName == 'إعلاناتي') {
                    OpenLongTouchPopup('MyAdv', advertisement.id);
                }
                else if ($scope.pageName == 'قائمة المفضلة') {
                    OpenLongTouchPopup('Fav', advertisement.id);
                }
                else {

                }
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true;    cordova.plugins.notification.badge.clear();
        };

        $scope.GoToAdvertisementDetails = function (advertisement) {
            if ($scope.allAdvertisementsLoaded == true) {
                $rootScope.advId = advertisement.id;
                helpers.GoToPage('advertisementsDetails', { advertisement: advertisement });
            }
            
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };

        app.init();
    });

}]);

