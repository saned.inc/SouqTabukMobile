﻿
(function () { 

    myApp.angular.controller("mapCtrl", ["$http", "$rootScope", "$scope", "helpers", function ($http, $rootScope, $scope, helpers) {
        var fw7 = myApp.fw7;
        var app = myApp.fw7.app;
        app.onPageBeforeAnimation('map', function (page) {
            if ($rootScope.lat && $rootScope.lng) {
                $rootScope.initMap(parseInt($rootScope.lat), parseInt($rootScope.lng));
            }
            else {
                $rootScope.initMap(24.713552, 46.675296);
            }

        });

         app.onPageAfterAnimation('map', function (page) {

        });

        $rootScope.initMap = function (lat, lng) {

            document.addEventListener("deviceready", function () {
                var div = document.getElementById("map_canvas");
                var map;
                var uluru = { lat: lat, lng: lng };
                map = new google.maps.Map(document.getElementById('map_canvas'), {
                    zoom: 4,
                    center: uluru
                });
                marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
                new google.maps.event.addListener(map, 'click', function (event) {
                    $rootScope.lat = event.latLng.lat();
                    $rootScope.lng = event.latLng.lng();
                    nativegeocoder.reverseGeocode(success, failure, $rootScope.lat, $rootScope.lng, { defaultLocale: "en"});
                    function success(result) {
                        if (result[0]) {
                            var res = [];
                            res.push(result[0].countryName);
                            res.push(result[0].administrativeArea);
                            res.push(result[0].locality);
                            res.push(result[0].thoroughfare);
                            res = res.reverse();
                            $rootScope.address = res.toString();
                            $rootScope.signAddress = res.toString()
                            $rootScope.editUserProfileForm.editAddress = res.toString()
                            helpers.GoBack();
                            setTimeout(function () {
                                $rootScope.$apply();
                            },500)
                        }

                       
                    }
                    function failure(err) {
                        //callback(null)
                        //$rootScope.f7.router.back();
                    }
                });
            }, false);

        }
    }]);

})();

