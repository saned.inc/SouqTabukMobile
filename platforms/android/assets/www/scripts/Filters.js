﻿myApp.angular.filter('generateURL', function ($http) {
    return function (image) {

        if (image != null && image != '' && image != ' ' && image.indexOf('img/') == -1 && image.indexOf('http://placehold.it') == -1) {
            if (image.length > 200) {
                return 'data:image/jpeg;base64, ' + image;
            }
            else if (image.indexOf('file://') > -1 || image.indexOf('Users') > -1 || image.indexOf('/var/') > -1 || image.indexOf('Applications/') > -1) {
                return image;
            }
            else {
                if (image.indexOf('http') > -1) {
                    return image;
                }
                else {
                    return hostUrl + image;
                }
            }
            
        }
        else {
            if (image != null && image != '' && image != ' ' && (image.indexOf('puff.svg') > -1 || image.indexOf('avatar.jpg') > -1)) {
                return image;
            }
            else {
                return 'http://none.com';
            }
        }
    };
});