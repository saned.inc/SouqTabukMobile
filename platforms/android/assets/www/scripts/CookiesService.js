﻿myApp.angular.factory('CookieService', [function () {
    'use strict';

    var getCookie = function getCookie(cookieName) {
        return localStorage.getItem('SouqTabuk_' + cookieName) != "undefined" ? localStorage.getItem('SouqTabuk_' + cookieName) : null;
    }

    var setCookie = function setCookie(cookieName, cookieValue) {
        localStorage.setItem('SouqTabuk_' + cookieName, cookieValue);
    }

    var removeCookie = function (cookieName) {
        localStorage.removeItem('SouqTabuk_' + cookieName);
    }

    return {
        getCookie: getCookie,
        setCookie: setCookie,
        removeCookie: removeCookie
    };
}]);