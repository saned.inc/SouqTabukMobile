﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />
/// <reference path="../js/jquery-2.1.0.js" />

var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;

Template7.global = {
    android: isAndroid,
    ios: isIos
};

var $$ = Dom7;

if (isAndroid) {
    $$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');
    $$('.view .navbar').prependTo('.view .page');
}

if (isIos) {
    $$('.view.navbar-fixed').removeClass('navbar-fixed').addClass('navbar-through');
    $$('.view .navbar').prependTo('.view .page');
}

var myApp = {};

myApp.config = {
};

var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

myApp.angular = angular.module('myApp', ['ErrorCatcher', 'jkAngularRatingStars'])

    .config(function ($provide) {
    $provide.decorator("$exceptionHandler", function ($delegate, $injector, $log) {
        return function (exception, cause) {
            $log.error(exception)
            $delegate(exception, cause);
        };
    });
})
    .directive("limitTo", [function () {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var limit = parseInt(attrs.limitTo);
                angular.element(elem).on("keypress", function (e) {
                    if (this.value.length == limit) e.preventDefault();
                });
            }
        }
    }]).directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    }).directive('longTouch', function () {
        return {
            scope: {
                longTouch: "&",
                time: "@",
                clickEvent: "&",
            },
            link: function (scope, element, attrs) {
                scope.timepassed = false;
                scope.x = 0;
                scope.y = 0;
                scope.moved = false;
                element.on('touchstart', function (event) {
                    scope.moved = false;
                    scope.timepassed = false;
                    if (event.originalEvent)
                        scope.x = event.originalEvent.touches[0].clientX;
                    else
                        scope.x = event.touches[0].clientX;

                    if (event.originalEvent)
                        scope.y = event.originalEvent.touches[0].clientY;
                    else
                        scope.y = event.touches[0].clientY;
                    var time = 600;
                    if (isNaN(scope.time) == false) {
                        if (Number.isInteger(scope.time))
                            time = scope.time;
                        else
                            time = Number(scope.time);
                    }

                    setTimeout(function () {
                        console.log('settitmeout in touch start');
                        scope.timepassed = true;
                        scope.$apply();
                    }, time);


                });
                element.on('touchcancel', function (event) {
                    scope.timepassed = false;

                });
                element.on('touchmove', function (event) {
                    var x = 0;
                    var y = 0;
                    if (event.originalEvent)
                        x = event.originalEvent.touches[0].clientX;
                    else
                        x = event.touches[0].clientX;
                    if (event.originalEvent)
                        y = event.originalEvent.touches[0].clientY;
                    else
                        y = event.touches[0].clientY;
                    if (x - scope.x > 10 || x - scope.x < -10 || y - scope.y < 10 || y - scope.y > 10) {
                        scope.timepassed = false;
                        scope.moved = true;
                    }
                });
                element.on('touchend', function (event) {
                    if (scope.timepassed && scope.longTouch) {
                        console.log('longTouch');
                        scope.longTouch();
                    }
                    else if (!scope.timepassed && scope.clickEvent && scope.moved == false) {
                        console.log('clickEvent');
                        scope.clickEvent();
                    }

                    console.log(' touch end');
                    scope.timepassed = false;
                });
            }
        };
    }).directive('homePage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'homeController',
            scope: {},
            templateUrl: 'pages/home.html'
        };
    })
    .directive('mapPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'mapCtrl',
            scope: {},
            templateUrl: 'pages/map.html'
        };
    }).directive('landingPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'landingController',
            scope: {},
            templateUrl: 'pages/landing.html'
        };
    }).directive('connectPage', function () {
        return {
            restrict: 'E',
            bindToController: false,
            controller: '',
            scope: {},
            templateUrl: 'pages/connect.html'
        };
    }).directive('advancedSearchPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'advancedSearchController',
            scope: {},
            templateUrl: 'pages/advancedSearch.html'
        };
    })
    .directive('loginPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'loginController',
            scope: {},
            templateUrl: 'pages/login.html'
        };
    }).directive('signupUserPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'signupUserController',
            scope: {},
            templateUrl: 'pages/signupUser.html'
        };
    }).directive('forgetPassPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'forgetPasswordController',
            scope: {},
            templateUrl: 'pages/forgetPassword.html'
        };
    }).directive('resetPasswordPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'resetPasswordController',
            scope: {},
            templateUrl: 'pages/resetPassword.html'
        };
    }).directive('changePassPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'changePasswordController',
            scope: {},
            templateUrl: 'pages/changePassword.html'
        };
    }).directive('userProfilePage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'userProfileController',
            scope: {},
            templateUrl: 'pages/userProfile.html'
        };
    }).directive('contactPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'contactController',
            scope: {},
            templateUrl: 'pages/contact.html'
        };
    }).directive('editProfilePage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'editProfileController',
            scope: {},
            templateUrl: 'pages/editProfile.html'
        };
    }).directive('userNotificationPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'userNotificationController',
            scope: {},
            templateUrl: 'pages/userNotification.html'
        };
    }).directive('noResultPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'noResultController',
            scope: {},
            templateUrl: 'pages/noResult.html'
        };
    }).directive('searchPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'searchController',
            scope: {},
            templateUrl: 'pages/search.html'
        };
    }).directive('termsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'termsController',
            scope: {},
            templateUrl: 'pages/terms.html'
        };
    }).directive('searchResultsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'searchResultsController',
            scope: {},
            templateUrl: 'pages/searchResults.html'
        };
    }).directive('addAdvertisementPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'addAdvertisementController',
            scope: {},
            templateUrl: 'pages/addAdvertisement.html'
        };
    }).directive('advertisementsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'advertisementsController',
            scope: {},
            templateUrl: 'pages/advertisements.html'
        };
    }).directive('advertisementDetailsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'advertisementsDetailsController',
            scope: {},
            templateUrl: 'pages/advertisementsDetails.html'
        };
    }).directive('commentsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'commentsController',
            scope: {},
            templateUrl: 'pages/comments.html'
        };
    }).directive('messagesPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'messagesController',
            scope: {},
            templateUrl: 'pages/messages.html'
        };
    }).directive('aboutPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'aboutController',
            scope: {},
            templateUrl: 'pages/about.html'
        };
    }).directive('bankAccountPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'bankAccountController',
            scope: {},
            templateUrl: 'pages/bankAccount.html'
        };
    }).directive('bannedGoodsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'bannedGoodsController',
            scope: {},
            templateUrl: 'pages/bannedGoods.html'
        };
    }).directive('commissionPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'commissionController',
            scope: {},
            templateUrl: 'pages/commission.html'
        };
    }).directive('chatPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'chatController',
            scope: {},
            templateUrl: 'pages/chat.html'
        };
    }).directive('userDetailsPage', function () {
        return {
            restrict: 'E',
            bindToController: true,
            controller: 'userDetailsController',
            scope: {},
            templateUrl: 'pages/userDetails.html'
        };
    });

//myApp.angular.run(function () {
//    alert("get");
//});

myApp.angular.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    }
    return fallbackSrc;
});

myApp.fw7 = {
    app: new Framework7({
        swipeBackPage: false,
        swipePanel: false,
        panelsCloseByOutside: true,
        animateNavBackIcon: true,
        material: isAndroid ? true : false,
        materialRipple: false,
        //uniqueHistory:true,
        modalButtonOk: 'تم',
        modalButtonCancel: 'إلغاء'
    }),
    options: {
        dynamicNavbar: false,
        domCache: true
    },
    views: [],
    Countries: [],
    Cities: [],
    Categories: [],
    Advertisements: [],
    DelayBeforeScopeApply: 10,
    PageSize: 50
};