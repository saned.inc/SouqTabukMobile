﻿myApp.angular.factory('SidePanelService', ['$rootScope', 'CookieService', 'appServices', function ($rootScope, CookieService, appServices) {
    'use strict';

    function DrawMenu() {
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var loginUsingSocial = CookieService.getCookie('loginUsingSocial') ? CookieService.getCookie('loginUsingSocial') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;
        $rootScope.IsVisitor = IsVisitor;

        var fw7 = myApp.fw7;

        if (IsVisitor == 'true') {
            $('#linkMenuToHome').css('display', 'block ');
            $('#linkMenuToMyMessages').css('display', 'none');
            $('#spanNewMessages').css('display', 'none');
            $('#linkMenuToMyFavorites').css('display', 'none');
            $('#linkMenuToAppCommission').css('display', 'block');
            $('#linkMenuToBankAccounts').css('display', 'block');
            $('#linkMenuToTerms').css('display', 'block');
            $('#linkMenuToAboutUs').css('display', 'block');
            $('#linkMenuToChangePassword').css('display', 'none');
            $('#linkMenuToContact').css('display', 'none');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToProfile').css('display', 'none');
            $('#lblMenuLoginText').html('تسجيل دخول');
            $$('#imgSideMenu').attr('src', 'img/logo.png');
        }
        else {
            $('#linkMenuToHome').css('display', 'block');
            $('#linkMenuToMyMessages').css('display', 'block');
            $('#spanNewMessages').css('display', 'block');
            $('#linkMenuToMyFavorites').css('display', 'block');
            $('#linkMenuToAppCommission').css('display', 'block');
            $('#linkMenuToBankAccounts').css('display', 'block');
            $('#linkMenuToTerms').css('display', 'block');
            $('#linkMenuToAboutUs').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToProfile').css('display', 'block');
                if (loginUsingSocial == 'true') {
                    $('#linkMenuToChangePassword').css('display', 'none');
                }
                else {
                    $('#linkMenuToChangePassword').css('display', 'block');
                }
                $('#lblMenuLoginText').html('تسجيل خروج');
            }
            else {
                $('#linkMenuToProfile').css('display', 'none');
                $('#linkMenuToChangePassword').css('display', 'none');
                $('#lblMenuLoginText').html('تسجيل دخول');
            }

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                if (typeof userLoggedIn.image != 'undefined' && userLoggedIn.image != null && userLoggedIn.image != '' && userLoggedIn.image != ' ') {
                    if (userLoggedIn.image.indexOf('http://') > -1 || userLoggedIn.image.indexOf('https://') > -1 || userLoggedIn.image.indexOf('http://placehold.it') > -1 ||
                        userLoggedIn.image.indexOf('https://placehold.it') > -1) {
                        $$('#imgSideMenu').attr('src', userLoggedIn.image);
                    }
                    else {
                        if (userLoggedIn.image.indexOf('placehold.it') > -1) {
                            $$('#imgSideMenu').attr('src', 'http:' + userLoggedIn.image);
                        }
                        else {
                            $$('#imgSideMenu').attr('src', hostUrl + userLoggedIn.image);
                        }
                    }
                }
                else {
                    $$('#imgSideMenu').attr('src', 'img/logo.png');
                }
            }
            else {
                $$('#imgSideMenu').attr('src', 'img/logo.png');
            }
        }

        setTimeout(function () {
            //$scope.$apply();
            // $rootScope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    return {
        DrawMenu: DrawMenu
    }

}]);