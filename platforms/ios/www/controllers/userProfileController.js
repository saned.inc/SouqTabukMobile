﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var advCount;
    var favCount;

    function LoadUserData(user) {
        $scope.IsUserNameExists = typeof user != 'undefined' && user.username != null && user.username != '' && user.username != ' ' ? true : false;
        $scope.IsMobileExists = typeof user != 'undefined' && user.phone != null && user.phone != '' && user.phone != ' ' ? true : false;
        $scope.IsEmailExists = typeof user != 'undefined' && user.email != null && user.email != '' && user.email != ' ' ? true : false;
        //$scope.IsCityExists = typeof user != 'undefined' && user.city != null && user.city.name != '' && user.city.name != ' ' ? true : false;
        $scope.IsCityExists = true;

        $scope.cityName = typeof user != 'undefined' && user.city != null && user.city.name != '' && user.city.name != ' ' ? user.city.name : 'لا يوجد';

        if (typeof user.image != 'undefined' && user.image != null && user.image != '' && user.image != ' ') {
            $scope.photoUrl = user.image;
        }
        else {
            $scope.photoUrl = 'img/avatar.jpg';
        }

        $scope.isLoaded = true;
    }

    $('#ProfileNotificationsLoaded').hide();
    $('#ProfileNotificationsNotLoaded').show();

    $(document).ready(function () {

        app.onPageInit('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

        });

        app.onPageBeforeAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            $scope.isLoaded = false;

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            $rootScope.RemoveEditPagesFromHistory();

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            //app.showIndicator();
            appServices.CallService('userProfile', 'GET', "api/v1/profile", '', function (userData) {
                app.hideIndicator();
                if (userData) {
                    if (userData.lat != null && userData.lng != null) {
                        $rootScope.latUpdate = userData.lat;
                        $rootScope.lngUpdate = userData.lng;
                    }
                    advCount = userData.advertisementCount;
                    favCount = userData.favoritesCount;
                    $scope.cityName = typeof userData != 'undefined' && userData.city != null && userData.city.name != '' && userData.city.name != ' ' ? userData.city.name : 'لا يوجد';

                    $scope.user = userData;
                    CookieService.setCookie('userLoggedIn', JSON.stringify(userData));
                    LoadUserData(userData);
                }
            });

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#ProfileNotificationsLoaded').show();
                    $('#ProfileNotificationsNotLoaded').hide();
                });
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';           
        
        });

        app.onPageReinit('userProfile', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        $scope.ChangeMobileSettingsForUser = function () {
            var profileCheckbox = document.getElementById('profileCheckbox');
            var isProfileChecked = profileCheckbox.checked ? false : true;

            CookieService.setCookie('showQuestionOwnerName', isProfileChecked);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true;    cordova.plugins.notification.badge.clear();
        };

        $scope.GoToEditProfile = function (user) {
            helpers.GoToPage('editProfile', { user: user });
        };

        $scope.GoToMyAdvertisements = function (user) {
            helpers.GoToPage('advertisements', { methodName: 'api/v1/myadvs', children: advCount });
        };

        $scope.GoToMyMessages = function (user) {
            helpers.GoToPage('messages', { user: user });
        };

        $scope.GoToMyFavorites = function (user) {
            helpers.GoToPage('advertisements', { methodName: 'api/v1/favorites', children: favCount });
        };

        $scope.GoToMyNotifications = function (user) {
            helpers.GoToPage('userNotification', { user: user });
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };

        app.init();
    });

}]);

