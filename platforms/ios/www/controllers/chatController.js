﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('chatController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var conversation_Id;
    var advertisementId;
    var isPull = true;
    var chatMessages;
    function GetNewMessagesFromPusher() {
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
        var pusherChannel = $rootScope.channel;

        pusherChannel.bind('App\\Events\\AddNewMessage', function (e) {
            try {
                $rootScope.NewNotificationsNumber = parseInt(parseInt(e.general.notifyCount) + parseInt(e.general.messageCount));
                $('#spanNewMessages').html(e.general.messageCount);
                $scope.noChatMessages = false;
                var newMessage = {};
                newMessage.user = {};
                newMessage.user_id = e.data.message.user_id;
                newMessage.message = e.data.message.message;
                if (typeof e.user.image != 'undefined' && e.user.image != null && e.user.image != '' && e.user.image != ' ') {
                    newMessage.user.image = e.user.image;
                }
                else {
                    newMessage.user.image = 'img/1.jpg';
                }
                newMessage.user.username = e.user.name;

                var filteredMessage = $scope.privateMessages.filter(function (obj) {
                    return obj.message.indexOf(newMessage.message) > -1;
                })[0];

                if (typeof filteredMessage == 'undefined' || filteredMessage == null) {
                    $scope.privateMessages.push(newMessage);
                }

                
                $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
                setTimeout(function () {
                    $scope.$apply();
                    $rootScope.$apply();
                }, fw7.DelayBeforeScopeApply);
            } catch (e) {
                console.log(e)
            }
        });
    }

    function LoadAdvertisementDetails(advertisementId, callBack) {
        if (!isPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
           // app.showIndicator();
        }

        appServices.CallService('advertisementsDetails', 'GET', "api/v1/advertisement/details?advId=" + advertisementId, '', function (advertisement) {
            $scope.isLoaded = true;
            if (advertisement != null && (typeof advertisement.data == 'undefined' || advertisement.data == null) && advertisement.message != 'this advertisement is not found.') {
                $scope.isDeleted = false;
                callBack(advertisement);
            }
            else {
                if (advertisement.message == 'this advertisement is not found.') {
                    $scope.isDeleted = true;
                    callBack(advertisement);
                }
                else {
                    $scope.isDeleted = false;
                    callBack(null);
                }
            }
        });
    }

    function LoadChatMessages(conversation_Id, callBack) {

     //   appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + conversation_Id, '', function (chatMessages) {
            if (!isPull) {
                app.hideIndicator();
            }

            if (chatMessages && chatMessages.length > 0) {
                $scope.noChatMessages = typeof chatMessages != 'undefined' && chatMessages != null && chatMessages.length > 0 ? false : true;

                angular.forEach(chatMessages, function (message) {
                    if (typeof message.user.image != 'undefined' && message.user.image != null && message.user.image != '' && message.user.image != ' ') {
                        message.user.image = message.user.image;
                    }
                    else {
                        message.user.image = 'img/1.jpg';
                    }
                });

                $scope.privateMessages = chatMessages;
            }
            else {
                $scope.noChatMessages = true;
                $scope.privateMessages = [];
            }

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
                $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
            }, fw7.DelayBeforeScopeApply);
       // });
    }

    function SetUserOffline(callBack) {

        var params = {
            'convId': conversation_Id
        };

        appServices.CallService('chat', 'POST', "api/v1/conversation/offline", params, function (chatResult) {
            callBack(true);
        });
    }

    $('#ChatNotificationsLoaded').hide();
    $('#ChatNotificationsNotLoaded').show();

    $(document).ready(function () {
        

        app.onPageInit('chat', function (page) {
            if ($rootScope.currentOpeningPage != 'chat') return;
            $rootScope.currentOpeningPage = 'chat';


            $$('#chatInfinite').on('ptr:refresh', function (e) {
                isPull = true;

                var IsVisitor = $rootScope.isVisitor();
                if (IsVisitor == true) {
                    $scope.IsVisitor = true;
                    $('.spanNotifications').hide();
                }
                else {
                    $scope.IsVisitor = false;
                    $('#ChatNotificationsLoaded').hide();
                    $('#ChatNotificationsNotLoaded').show();
                    $rootScope.CheckNotifications(function (notificationLength) {
                        $('#ChatNotificationsLoaded').show();
                        $('#ChatNotificationsNotLoaded').hide();
                    });
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

                if ($scope.privateMessages.length > 0) {

                    var skipCount = $scope.privateMessages.length;

                    appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + conversation_Id + '&&skipCount=' + skipCount, '', function (messages) {
                        if (messages) {
                            angular.forEach(messages, function (message) {
                                if (typeof message.user.image != 'undefined' && message.user.image != null && message.user.image != '' && message.user.image != ' ') {
                                    message.user.image = message.user.image;
                                }
                                else {
                                    message.user.image = 'img/1.jpg';
                                }

                                var filteredMessage = $scope.privateMessages.filter(function (obj) {
                                    return obj.message.indexOf(message.message) > -1;
                                })[0];

                                if (typeof filteredMessage == 'undefined' || filteredMessage == null) {
                                    $scope.privateMessages.unshift(message);
                                }
                            })
                            setTimeout(function () {
                                app.pullToRefreshDone();
                            }, 10);
                        }

                        setTimeout(function () {
                            $scope.isLoaded = true;
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    });
                }
            });

        });

        app.onPageBeforeAnimation('chat', function (page) {
            if ($rootScope.currentOpeningPage != 'chat') return;
            $rootScope.currentOpeningPage = 'chat';

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            $('#txtMessage').val('');
            $scope.privateMessages = [];
            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#ChatNotificationsLoaded').show();
                    $('#ChatNotificationsNotLoaded').hide();
                });
            }

            isPull = false;
            $scope.isLoaded = false;

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            $rootScope.RemoveEditPagesFromHistory();

            
            $scope.userLoggedInId = userLoggedIn.id;

            conversation_Id = page.query.conversation_Id;
            advertisementId = page.query.adv_id;
            chatMessages=[]
            chatMessages = page.query.messages;
            app.initPullToRefresh('#chatInfinite');

            if (userLoggedIn) {
                GetNewMessagesFromPusher();
            }

            LoadChatMessages(conversation_Id, function (result) {
                $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
            });
            //LoadAdvertisementDetails(advertisementId, function (advertisement) {
            //    if (advertisement != null && (typeof advertisement.data == 'undefined' || advertisement.data == null) && advertisement.message != 'this advertisement is not found.') {
            //        $scope.isSold = typeof advertisement.is_sold != 'undefined' && advertisement.is_sold != null && advertisement.is_sold == 1 || advertisement.is_sold == '1' ? true : false;

            //        LoadChatMessages(conversation_Id, function (result) {
            //            $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
            //        });
            //    }
            //    else {
            //        if (advertisement != null && advertisement.message == 'this advertisement is not found.') {
            //            $scope.isSold = false;
            //            if (!isPull) {
            //                app.hideIndicator();
            //            }
            //        }
            //        else {
            //            $scope.isSold = false;
            //            LoadChatMessages(conversation_Id, function (result) {
            //                $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
            //            });
            //        }
                    
            //    }
            //});

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('chat', function (page) {
            isPull = false;

            $('#txtMessage').val('');

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('chat', function (page) {
            if ($rootScope.currentOpeningPage != 'chat') return;
            $rootScope.currentOpeningPage = 'chat';

        });

        app.onPageBack('chat', function (page) {
            SetUserOffline(function (result) { });
        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToHome = function () {
            SetUserOffline(function (result) { });
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
            SetUserOffline(function (result) {});
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            SetUserOffline(function (result) { });
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true;    cordova.plugins.notification.badge.clear();
        };

        $scope.sendMessage = function () {
            var messageValue = $scope.message;

            if (messageValue != null && messageValue != "" && typeof messageValue != 'undefined') {
                $scope.messageLoading = true;

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
               // app.showIndicator();

                var params = {
                    'convId': conversation_Id,
                    'message': messageValue
                };

                appServices.CallService('chat', 'POST', "api/v1/conversations", params, function (chatResult) {
                    app.hideIndicator();
                    $scope.messageLoading = false;

                    if (chatResult) {
                        var newMessage = {
                            message: messageValue,
                            conversation_id: conversation_Id,
                            user_id: $scope.userLoggedInId,
                            dateDiff: 0
                        }

                        if ($scope.privateMessages) {
                            $scope.privateMessages.push(newMessage);
                        }
                        $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
                        $scope.message = "";
                    }

                    setTimeout(function () {
                        $('#chatInfinite').animate({ scrollTop: $('#chatInfinite')[0].scrollHeight }, "slow");
                    }, 10);
                });
            }
            else {
                language.openFrameworkModal('خطا', 'قم بادخال رسالة .', 'alert', function () { });
            }
        }

        app.init();
    });

}]);

