﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('loginController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var pageName = '';

    function OpenActivationCodePopup() {
        var activation = app.modal({
            title: 'كود التفعيل',
            text: '',
            afterText: '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="number" name="phone" placeholder="كود التفعيل">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>',
            buttons: [
                {
                    text: 'تفعيل',
                    onClick: function () {
                        var code = $('#txtCode').val();
                        var mobile = CookieService.getCookie('confirmationMobile');

                        var params = {
                            'activation_code': code
                        };

                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                       // app.showIndicator();
                        appServices.CallService('activation', "POST", 'api/v1/activation', params, function (res) {
                            app.hideIndicator();
                            app.closeModal(activation);

                            if (res != null) {
                                language.openFrameworkModal('نجاح', 'تم التأكد من الجوال بنجاح .', 'alert', function () {
                                    CookieService.setCookie('appToken', res.api_token);
                                    CookieService.setCookie('USName', res.username);
                                    CookieService.setCookie('Visitor', false);
                                    CookieService.setCookie('loginUsingSocial', false);
                                    CookieService.setCookie('UserEntersCode', 'true');
                                    if (res.image) {
                                        $rootScope.userImage = res.image;
                                    }
                                    app.closeModal(activation);
                                    CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                                    helpers.GoToPage('home', null);
                                });
                            }
                            else {
                                language.openFrameworkModal('خطأ', 'كود التفعيل غير صحيح .', 'alert', function () {
                                    
                                });

                                OpenActivationCodePopup();
                            }
                        });
                    }
                },
                {
                    text: 'إعادة إرسال الكود',
                    onClick: function () {
                        var mobile = CookieService.getCookie('confirmationMobile');

                        var params = {
                            'phone': mobile,
                            'new_phone':mobile 
                        };

                        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                        //app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/v1/activation/resend', params, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                OpenActivationCodePopup()
                                language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () {
                                    // app.openModal(activation);
                                });

                            }
                            else {
                                OpenActivationCodePopup()
                               // app.openModal(activation);
                            }
                        });
                    }
                },
                {
                    text: 'إلغاء',
                    onClick: function () {
                        app.closeModal(activation);
                    }
                }
            ]
        })
    }

    function EnterApplication() {
        if (CookieService.getCookie('userLoggedIn') || CookieService.getCookie('UserID')) {
            if (CookieService.getCookie('UserEntersCode') == "false") {
                if (CookieService.getCookie('loginUsingSocial') == 'true') {
                    helpers.GoToPage('home', null);
                } else {
                    OpenActivationCodePopup();
                }
            }
            else {
                helpers.GoToPage('home', null);
            }
        }
        else {
            if (CookieService.getCookie('UID')) {
                helpers.GoToPage('signupUser', null);
            }
            else {
                helpers.GoToPage('login', null);
            }
        }
    }


    //EnterApplication();


    $(document).ready(function () {

        app.onPageBeforeInit('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';
        });

        app.onPageInit('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';

            $scope.resetForm();
        });

        app.onPageBeforeAnimation('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';

            $scope.resetForm();
        });

        app.onPageAfterAnimation('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';
            $scope.resetForm();

            pageName = page.fromPage.name;
            navigator.splashscreen.hide();
        });

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

    $scope.resetForm = function () {
        $scope.loginReset = false;
        $scope.loginForm.mobile = null;
        $scope.loginForm.password = null;

        if (typeof $scope.LoginForm != 'undefined' && $scope.LoginForm != null) {
            $scope.LoginForm.$setPristine(true);
            $scope.LoginForm.$setUntouched();
        }

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    function RegisterDevice(callBack) {
        helpers.RegisterDevice(true, function (result) {
            callBack(true);
        });
     //callBack(true);
    }

    $scope.PassToHome = function () {
        helpers.RegisterDevice(false, function (result) {
            CookieService.setCookie('Visitor', true);
            CookieService.removeCookie('userLoggedIn');
            SidePanelService.DrawMenu();
            helpers.GoToPage('home', null);
        });
    };

    $scope.GoToSignUp = function () {
        helpers.GoToPage('signupUser', null);
    };

    $scope.GoToForgetPassword = function () {
        helpers.GoToPage('forgetPass', null);
    };

    $scope.form = {};
    $scope.loginForm = {};
    $scope.submitForm = function (isValid) {
        $scope.loginReset = true;

        if (isValid) {
            var loginEmail = $scope.loginForm.mobile, loginPass = $scope.loginForm.password;

            RegisterDevice(function (result) {
                var deviceId = CookieService.getCookie('deviceId');
                
                appServices.CallService('login', "POST", "api/v1/login?phone=" + loginEmail + "&password=" + loginPass + "&playerId=" + deviceId, '', function (resToken) {
                    app.hideIndicator();
                    if (resToken != null && resToken != 'notActive') {
                        CookieService.setCookie('appToken', resToken.api_token);
                        CookieService.setCookie('USName', resToken.username);
                        CookieService.setCookie('Visitor', false);
                        CookieService.setCookie('loginUsingSocial', false);
                        CookieService.setCookie('UserEntersCode', 'true');
                        CookieService.setCookie('userLoggedIn', JSON.stringify(resToken));
                        SidePanelService.DrawMenu();
                        if (typeof resToken.image != 'undefined' && resToken.image != null && resToken.image != '' && resToken.image != ' ') {
                            CookieService.setCookie('usrPhoto', resToken.image);
                        }
                        else {
                            CookieService.removeCookie('usrPhoto');
                        }

                        helpers.GoToPage('home', null);
                    }
                    else {
                        if (resToken == 'notActive') {
                            CookieService.setCookie('confirmationMobile',loginEmail);
                            OpenActivationCodePopup();
                        }
                    }
                });
            });
        }
    };

    $scope.loginWithFacebook = function () {
        var fbLoginFailed = function (error) {
            
        }

        var fbLoginSuccess = function (userData) {
            if (userData.authResponse) {
                var deviceId = CookieService.getCookie('deviceId');
                var accessToken = userData.authResponse.accessToken;
                var userFullName = '';
                var userEmail = '';

                facebookConnectPlugin.api(userData.authResponse.userID + "/?fields=id,email,first_name,last_name", ["public_profile"],
                    function (result) {
                        userFullName = result.first_name + ' ' + result.last_name;

                        CookieService.setCookie('socialEmail', result.email);
                        var registerObj = {
                            "social_provider": "Facebook",
                            "social_key": userData.authResponse.userID,
                            "name": userFullName,
                            'email': result.email,
                            'image': 'https://graph.facebook.com/' + userData.authResponse.userID + '/picture?type=large',
                            'username': userFullName
                        };

                        RegisterDevice(function (result) {
                            var deviceId = CookieService.getCookie('deviceId');
                            registerObj.deviceId = deviceId;
                            appServices.CallService('login', "POST", "api/v1/social/login?playerId="+deviceId, registerObj, function (res) {
                                app.hideIndicator();
                                if (res != null) {
                                    CookieService.setCookie('USName', res.username);
                                    CookieService.setCookie('appToken', res.api_token);
                                    CookieService.setCookie('Visitor', false);
                                    CookieService.setCookie('loginUsingSocial', true);
                                    CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                                    if (typeof res.image != 'undefined' && res.image != null && res.image != '' && res.image != ' ') {
                                        CookieService.setCookie('usrPhoto', res.image);
                                    }
                                    else {
                                        CookieService.removeCookie('usrPhoto');
                                    }
                                    SidePanelService.DrawMenu();
                                    helpers.GoToPage('home', null);
                                }
                            });
                        });
                    },
                    function (error) {
                        console.log('ERROR:' + error);
                    });
            }
        }

        facebookConnectPlugin.login(["public_profile", "email"], fbLoginSuccess, fbLoginFailed);

    };

    $scope.loginWithTwitter = function () {
        TwitterConnect.login(function (data) {
            var accessToken = data.token;
            var userFullName = '';

            TwitterConnect.showUser(function (result) {
                var userFullName = result.name;
                var deviceId = CookieService.getCookie('deviceId');

                var registerObj = {
                    "social_provider": "Twitter",
                    "social_key": data.userId,
                    "name": userFullName,
                    'email': '',
                    'image': result.profile_image_url,
                    'username': userFullName,
                };

                RegisterDevice(function (result) {
                    var deviceId = CookieService.getCookie('deviceId');
                    registerObj.deviceId = deviceId;
                    appServices.CallService('login', "POST", "api/v1/social/login?playerId="+deviceId, registerObj, function (res) {
                        app.hideIndicator();
                        if (res != null) {
                            CookieService.setCookie('USName', res.userName);
                            CookieService.setCookie('appToken', res.api_token);
                            CookieService.setCookie('Visitor', false);
                            CookieService.setCookie('loginUsingSocial', true);
                            CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                            if (typeof res.image != 'undefined' && res.image != null && res.image != '' && res.image != ' ') {
                                CookieService.setCookie('usrPhoto', res.image);
                            }
                            else {
                                CookieService.removeCookie('usrPhoto');
                            }
                            SidePanelService.DrawMenu();
                            helpers.GoToPage('home', null);
                        }
                    });
                });

            }, function (error) {
                language.openFrameworkModal('خطأ', 'خطأ في إسترجاع بيانات المستخدم', 'alert', function () { });
            });



        }, function (error) {
            console.log('Error in Login');
        });

    };

    $scope.loginWithGooglePlus = function () {
        window.plugins.googleplus.login(
                {
                    'scopes': 'profile email',
                    'webClientId': '1026744162651-eel8p51gvstv2uh12cn5h4oeuceop414.apps.googleusercontent.com',
                    'offline': false,
                },
                function (obj) {
                    var userName = obj.displayName;
                    var userEmail = obj.email;
                    var userId = obj.userId;
                    var userToken = obj.idToken;
                    var userPicture = obj.imageUrl;

                    var deviceId = CookieService.getCookie('deviceId');

                    CookieService.setCookie('socialEmail', userEmail);

                    var registerObj = {
                        "social_provider": "Google",
                        "social_key": userId,
                        "name": userName,
                        "email": userEmail,
                        'image': userPicture,
                        'username': userName,
                    };

                    RegisterDevice(function (result) {
                        var deviceId = CookieService.getCookie('deviceId');
                        registerObj.deviceId = deviceId;
                        appServices.CallService('login', "POST", "api/v1/social/login?playerId="+deviceId, registerObj, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                CookieService.setCookie('USName', res.username);
                                CookieService.setCookie('appToken', res.api_token);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginUsingSocial', true);
                                CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                                if (typeof res.image != 'undefined' && res.image != null && res.image != '' && res.image != ' ') {
                                    CookieService.setCookie('usrPhoto', res.image);
                                }
                                else {
                                    CookieService.removeCookie('usrPhoto');
                                }
                                SidePanelService.DrawMenu();
                                helpers.GoToPage('home', null);
                            }
                        });
                    });
                },
                function (msg) {
                    console.log('error: ' + msg);
                });
    };

}]);

