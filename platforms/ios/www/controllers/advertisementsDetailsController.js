﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('advertisementsDetailsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var isPull = true;
    var allComments = []
    var myPhotoBrowserPopupDark;
    var advertisement;
    var loadingComments;
    var watchID;
    var appToken;
    $scope.form = {};
    $scope.commentForm = {};

    // function onSuccess(position) {
    //     var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
    //     if ($rootScope.advId!=null && $rootScope.advLat != null && $rootScope.advLng!=null&& userLoggedIn!=null) {
    //         //$scope.distanceLoading=true
    //         appServices.CallService('comments', 'POST', 'api/v1/distance/bylatLng?api_token=' + appToken + '&advId=' + $rootScope.advId + '&lat=' + position.coords.latitude + '&lng=' + position.coords.longitude, '', function (result) {
    //             if (result != null) {
    //                 $scope.distanceLoading = false
    //                 $scope.advertisement.distance = result.distance
    //             }
    //             else {
    //                 $scope.distanceLoading = false
    //             }
    //         });
    //     }

    // }

    // function onError(error) {
    //     //alert('code: ' + error.code + '\n' +
    //     //    'message: ' + error.message + '\n');
    // }

   
    function LoadStaticAdvertisementData(advertisement, callBack) {
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var IsUserLogged = typeof userLoggedIn != 'undefined' && userLoggedIn != null && userLoggedIn != '' && userLoggedIn != ' ';

        if (IsUserLogged && advertisement.user != null) {
            //if (typeof userLoggedIn.image != 'undefined' && userLoggedIn.image != null && userLoggedIn.image != '' && userLoggedIn.image != ' ') {
            //    $scope.photoUrl = userLoggedIn.image;
            //}
            //else {
            //    $scope.photoUrl = 'img/avatar.jpg';
            //}
            $scope.phoneShow =  advertisement.user.phone == null ? false : true ; 
            advertisement.isAdvertisementOwner = userLoggedIn.id == advertisement.user.id ? true : false;
        }
        else {
            advertisement.isAdvertisementOwner = false;
        }


        var advertisementTotalRating = advertisement.rating != null ? parseInt(advertisement.rating) : 0;

        $scope.status = advertisement.is_sold == '0' ? 'إعلان جديد' : 'تم البيع';
        $scope.isSold = advertisement.is_sold == '0' ? false : true;

        $scope.totalRate = advertisementTotalRating;

        $scope.hasName = typeof advertisement.name != 'undefined' && advertisement.name != null && advertisement.name != '' && advertisement.name != ' ' ? true : false;
        $scope.hasDescription = typeof advertisement.description != 'undefined' && advertisement.description != null && advertisement.description != '' && advertisement.description != ' ' ? true : false;
        if (advertisement.user) {
            advertisement.user.phone = typeof advertisement.user.phone != 'undefined' && advertisement.user.phone != null && advertisement.user.phone != '' && advertisement.user.phone != ' ' ? advertisement.user.phone : 'لا يوجد';
        }
        $scope.advertisement = advertisement;

        setTimeout(function () {
            $scope.allAdvertisementsLoaded = true;
            $scope.$apply();
            callBack(true);
        }, fw7.DelayBeforeScopeApply);
    }

    function LoadAdvertisementDetails(advertisementId, callBack) {
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
        var status = 'إعلان جديد';

        appServices.CallService('advertisementsDetails', 'GET', "api/v1/advertisement/details?advId=" + advertisementId, '', function (advertisement) {
            $scope.isLoaded = true;
            $scope.distanceLoading = false
            if (advertisement != null && (typeof advertisement.data == 'undefined' || advertisement.data == null) && advertisement.message != 'this advertisement is not found.') {
                $rootScope.advLat = advertisement.lat;
               $scope.phoneShow =  advertisement.user.phone == null ? false : true ; 
                $rootScope.advLng = advertisement.lng;
                advertisement.day = advertisement.created_at.split(' ')[0];
                LoadComments(advertisement.id);
                $scope.noAdvertisementImages = typeof advertisement.images != 'undefined' && advertisement.images != null && advertisement.images.length > 0 ? false : true;
                $scope.hasMessage = advertisement.hasMessage;
                if (advertisement.user.image != null) {
                    $scope.photoUrl = advertisement.user.image;
                }
                else
                    $scope.photoUrl = 'img/avatar.jpg';
                $('.advNoResult').hide();
                $scope.isDeleted = false;

                var mainImage = { id: 0, url: advertisement.image, caption: advertisement.name };
                //advertisement.images.splice(0, 0, mainImage);

                var IsUserLogged = typeof userLoggedIn != 'undefined' && userLoggedIn != null && userLoggedIn != '' && userLoggedIn != ' ';

                if (IsUserLogged && advertisement.user != null) {
                    advertisement.isAdvertisementOwner = userLoggedIn.id == advertisement.user.id ? true : false;
                }
                else {
                    advertisement.isAdvertisementOwner = false;
                }

                $scope.favIconClass = advertisement.isFavorite == true ? 'ion-android-favorite' : 'ion-android-favorite-outline';
                $scope.isUserFavoritesBefore = advertisement.isFavorite;
                $scope.isUserFollowedBefore = advertisement.isFollowed;


                var advertisementTotalRating = advertisement.rating != null ? parseInt(advertisement.rating) : 0;

                $scope.status = advertisement.is_sold == '0' ? 'إعلان جديد' : 'تم البيع';
                $scope.isSold = advertisement.is_sold == '0' ? false : true;

                $scope.totalRate = advertisementTotalRating;

                $scope.hasName = typeof advertisement.name != 'undefined' && advertisement.name != null && advertisement.name != '' && advertisement.name != ' ' ? true : false;
                $scope.hasDescription = typeof advertisement.description != 'undefined' && advertisement.description != null && advertisement.description != '' && advertisement.description != ' ' ? true : false;
                advertisement.user.phone = typeof advertisement.user.phone != 'undefined' && advertisement.user.phone != null && advertisement.user.phone != '' && advertisement.user.phone != ' ' ? advertisement.user.phone : 'لا يوجد';
                // var mainImage={}
                // mainImage.url = advertisement.image
                // mainImage.advertisement_id =  advertisement.id
                // advertisement.images.push(mainImage)
                $scope.advertisement = advertisement;

                var photosArray = [];

                var photos = advertisement.images;
              

                angular.forEach(photos, function (photo) {
                    if (photo.url != null) {
                        photo.url = photo.url.indexOf('http') > -1 || photo.url.indexOf('Http') > -1 || photo.url.indexOf('https') > -1 ? photo.url : hostUrl + photo.url;

                        var photoObj = { url: photo.url, caption: advertisement.name };
                        photosArray.push(photoObj);
                    }

                });

                if (photosArray.length > 0) {
                    myPhotoBrowserPopupDark = app.photoBrowser({
                        photos: photosArray,
                        theme: 'dark',
                        backLinkText: 'إغلاق',
                        lazyLoading: true,
                        ofText: 'من',
                        type: 'popup'
                    });
                }

                setTimeout(function () {
                    $rootScope.initalSwiper('#AdvertisementDetailsSwiper', advertisement.images);
                }, 500);
            }
            else {
                if (advertisement.message == 'this advertisement is not found.') {
                    $('.advNoResult').show();
                    $scope.favIconClass = 'ion-android-favorite-outline';
                    $scope.isUserFavoritesBefore = false;
                    $scope.isUserFollowedBefore = false;
                    $scope.status = 'تم حذف الإعلان';
                    $scope.isSold = false;
                    $scope.totalRate = 0;
                    $scope.hasName = false;
                    $scope.hasDescription = false;
                    $scope.isDeleted = true;
                }
                else {
                    $('.advNoResult').show();
                    $scope.favIconClass = 'ion-android-favorite-outline';
                    $scope.isUserFavoritesBefore = false;
                    $scope.isUserFollowedBefore = false;
                    $scope.status = 'إعلان جديد';
                    $scope.isSold = false;
                    $scope.totalRate = 0;
                    $scope.hasName = false;
                    $scope.hasDescription = false;
                    $scope.isDeleted = false;
                }
            }

            setTimeout(function () {
                $scope.allAdvertisementsLoaded = true;
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

            callBack(true);


        });
    }

    function LoadComments(advertisementId) {
        CookieService.setCookie('comments-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var params = {
            'page': parseInt(CookieService.getCookie('comments-page-number')),
            'pageSize': myApp.fw7.PageSize
        };

        app.attachInfiniteScroll('#divInfiniteAdvertisementDetails');
        allComments = [];

        appServices.CallService('comments', 'GET', 'api/v1/comments/list?advId=' + advertisementId + '&&page=' + params.page + '&&pageSize=' + myApp.fw7.PageSize, '', function (result) {

            var response = result ? result.data : null;

            allComments = [];

            if (response && response.length > 0) {

                angular.forEach(response, function (comment) {
                    comment.created_at = comment.created_at.split('T')[0];

                    var IsUserLogged = typeof userLoggedIn != 'undefined' && userLoggedIn != null && userLoggedIn != '' && userLoggedIn != ' ';

                    if (IsUserLogged) {
                        comment.isCommentOwner = userLoggedIn.id == comment.user_id ? true : false;
                    }
                    else {
                        comment.isCommentOwner = false;
                    }

                    allComments.push(comment);
                });

                $scope.comments = allComments;
                $scope.noComments = allComments.length > 0 ? false : true;
            }
            else {
                $$('#divNoComments').addClass('partialNoResult');
                allComments = [];
                $scope.comments = [];
                $scope.noComments = true;
                $scope.commentsInfiniteLoader = false;
            }

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $('#advertisementDetailsNotificationsLoaded').hide();
    $('#advertisementDetailsNotificationsNotLoaded').show();

    $(document).ready(function () {
        app.onPageInit('advertisementsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'advertisementsDetails') return;
            $rootScope.currentOpeningPage = 'advertisementsDetails';
            $$('#divInfiniteAdvertisementDetails').on('ptr:refresh', function (e) {
                isPull = true;

                var IsVisitor = $rootScope.isVisitor();
                if (IsVisitor == true) {
                    $scope.IsVisitor = true;
                    $('.spanNotifications').hide();
                }
                else {
                    $scope.IsVisitor = false;
                    $rootScope.CheckNotifications(function (notificationLength) {
                        $('#advertisementDetailsNotificationsLoaded').show();
                        $('#advertisementDetailsNotificationsNotLoaded').hide();
                    });
                }

                var storedAdvertisement = JSON.parse(CookieService.getCookie('storedAdvertisement'));

                $scope.allAdvertisementsLoaded = false;

                LoadStaticAdvertisementData(storedAdvertisement, function (res) { });

                LoadAdvertisementDetails(storedAdvertisement.id, function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $$('#divInfiniteAdvertisementDetails').on('infinite', function () {
                if (loadingComments) return;
                loadingComments = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                var storedAdvertisement = JSON.parse(CookieService.getCookie('storedAdvertisement'));

                CookieService.setCookie('comments-page-number', parseInt(CookieService.getCookie('comments-page-number')) + 1);

                var params = {
                    'page': parseInt(CookieService.getCookie('comments-page-number')),
                    'pageSize': myApp.fw7.PageSize
                };

                appServices.CallService('comments', 'GET', 'api/v1/comments/list?advId=' + storedAdvertisement.id + '&&page=' + params.page + '&&pageSize=' + params.pageSize, '', function (result) {
                    var response = result ? result.data : null;

                    if (response && response.length > 0) {
                        loadingComments = false;

                        angular.forEach(response, function (comment) {
                            comment.created_at = comment.created_at.split('T')[0];

                            var IsUserLogged = typeof userLoggedIn != 'undefined' && userLoggedIn != null && userLoggedIn != '' && userLoggedIn != ' ';

                            if (IsUserLogged) {
                                comment.isCommentOwner = userLoggedIn.id == comment.user_id ? true : false;
                            }
                            else {
                                comment.isCommentOwner = false;
                            }

                            allComments.push(comment);
                        });

                        $scope.comments = allComments;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.commentsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteAdvertisementDetails');
                            return;
                        }
                    }
                    else {
                        $scope.commentsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteAdvertisementDetails');
                        loadingComments = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBack('advertisements', function (page) {
            $scope.shareMessageLoad = false;
        });
        app.onPageBeforeAnimation('advertisementsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'advertisementsDetails') return;
            $rootScope.currentOpeningPage = 'advertisementsDetails';
            $scope.distanceLoading = true;
            $scope.resetForm();
         
            advertisement = page.query.advertisement;
            CookieService.setCookie('storedAdvertisement', JSON.stringify(advertisement));

            isPull = false;
            $scope.usertotalRate = typeof advertisement.userRate != 'undefined' && advertisement.userRate != null ? advertisement.userRate : 0;
            $('.advNoResult').hide();

            app.initPullToRefresh('#divInfiniteAdvertisementDetails');

            $scope.isAdvertisementChecked = false;
            $("#chkAdvertisementDetails").prop("checked", $scope.isAdvertisementChecked);

            $scope.allAdvertisementsLoaded = false;

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#advertisementDetailsNotificationsLoaded').show();
                    $('#advertisementDetailsNotificationsNotLoaded').hide();
                });
            }

            $scope.isLoaded = true;
            $scope.shareMessageLoad = false;
            $scope.shareCallLoad = false;
            $scope.shareFavoriteLoad = false;
            $scope.shareWhatsLoad = false;

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            $rootScope.RemoveEditPagesFromHistory();

            LoadStaticAdvertisementData(advertisement, function (res) { });

            LoadAdvertisementDetails(advertisement.id, function (result) { });



            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('advertisementsDetails', function (page) {
            isPull = false;

            $scope.isAdvertisementChecked = false;
            $("#chkAdvertisementDetails").prop("checked", $scope.isAdvertisementChecked);

            $('.advNoResult').hide();

            if (page.fromPage.name == 'comments') {
                var advertisement = page.query.advertisement;
                $scope.allAdvertisementsLoaded = false;

                LoadStaticAdvertisementData(advertisement, function (res) { });

                LoadAdvertisementDetails(advertisement.id, function (result) { });
            }

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('advertisementsDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'advertisementsDetails') return;
            $rootScope.currentOpeningPage = 'advertisementsDetails';
            appToken = CookieService.getCookie('appToken');
            // if (appToken != null) {
            //     $rootScope.watchID = navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 60000, enableHighAccuracy: true });
            // }
            var advertisement = page.query.advertisement;
            CookieService.setCookie('storedAdvertisement', JSON.stringify(advertisement));

            $('.advNoResult').hide();

            isPull = false;

            app.initPullToRefresh('#divInfiniteAdvertisementDetails');

            $scope.isAdvertisementChecked = false;
            $("#chkAdvertisementDetails").prop("checked", $scope.isAdvertisementChecked);

            $scope.allAdvertisementsLoaded = false;

            LoadStaticAdvertisementData(advertisement, function (res) { });

            LoadAdvertisementDetails(advertisement.id, function (result) { });
        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true; cordova.plugins.notification.badge.clear();
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {

                    helpers.GoToPage('addAdvertisement', { isEdit: false });
                }
            });
        };

        $scope.GoToComments = function (advertisement) {
            helpers.GoToPage('comments', { advertisement: advertisement });
        };

        $rootScope.initalSwiper = function (id, advertisments) {
            var max = advertisments.length;
            var advId = $scope.advertisement.id;

            var swiper = $$('#mySwiper_' + advId)[0].swiper;
            if (swiper)
                swiper.destroy(true, true);

            $rootScope.swiper = app.swiper('#mySwiper_' + advId, {
                pagination: false,
                paginationHide: false,
                paginationClickable: true,
                spaceBetween: 0,
                disableOnInteraction: false,
                autoplay: 4000,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            });
        };

        $scope.CallAdvertisementOwner = function (advertisement) {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    if ($scope.shareCallLoad == true) return;

                    $scope.shareCallLoad = true;
                    var contactPhone = '+966';
                    contactPhone += typeof advertisement.user.phone != 'undefined' && advertisement.user.phone != null && advertisement.user.phone != 'لا يوجد'
                        && advertisement.user.phone != '' && advertisement.user.phone != ' ' ? parseInt(advertisement.user.phone, 10) : '';

                    window.plugins.CallNumber.callNumber(
                        function onSuccess(successResult) {
                            $scope.shareCallLoad = false;
                            $scope.$apply();
                        }, function onError(errorResult) {
                            $scope.shareCallLoad = false;
                            $scope.$apply();
                            console.log("Error:" + errorResult);
                        }, contactPhone, true);
                }
            });
        };

        $scope.GoToChat = function (advertisement) {
            var hasMessage = $scope.hasMessage;
            setTimeout(function () {
                $scope.shareMessageLoad = true
                $scope.$apply();
            }, 10);
            if (hasMessage == false) {
                //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                //  app.showIndicator();
                appServices.CallService('advertisementsDetails', 'POST', 'api/v1/conversations?advId=' + advertisement.id, '', function (result) {
                    app.hideIndicator();
                    if (result && result != null) {
                        $scope.hasMessage = false;
                        advertisement.conversation_id = result.id;
                        appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + advertisement.conversation_id, '', function (chatMessages) {
                            setTimeout(function () {
                                $scope.shareMessageLoad = false
                                $scope.$apply();
                            }, 100);
                                helpers.GoToPage('chat', { conversation_Id: advertisement.conversation_id, adv_id: advertisement.id, messages: chatMessages });
                        });
                  
                    }
                });
            }
            else {
                appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + advertisement.conversation_id, '', function (chatMessages) {
                    setTimeout(function () {
                        $scope.shareMessageLoad = false
                        $scope.$apply();
                    }, 100);
                    helpers.GoToPage('chat', { conversation_Id: advertisement.conversation_id, adv_id: advertisement.id, messages: chatMessages });
                });
               
            }
        };

        $scope.AddAdveritsementToFavroites = function (advertisement) {
            if ($scope.shareFavoriteLoad == true) return;

            var methodName = advertisement.isFavorite == true ? 'api/v1/unfavorite?advId=' + advertisement.id : 'api/v1/favorite?advId=' + advertisement.id;

            $scope.shareFavoriteLoad = true;

            appServices.CallService('advertisementsDetails', 'POST', methodName, '', function (result) {
                app.hideIndicator();
                if (result) {
                    $scope.favIconClass = methodName.indexOf('api/v1/favorite') > -1 ? 'ion-android-favorite' : 'ion-android-favorite-outline';
                    $scope.isUserFavoritesBefore = methodName.indexOf('api/v1/favorite') > -1 ? true : false;
                    $scope.advertisement.isFavorite = !$scope.advertisement.isFavorite;
                }

                $scope.shareFavoriteLoad = false;
                $scope.$apply();
            });
        };

        $scope.CallOwnerOnWhatsapp = function (advertisement) {
            if ($scope.callWhatsLoad == true) return;

            var contactPhone = '+966';
            contactPhone += typeof advertisement.user.phone != 'undefined' && advertisement.user.phone != null && advertisement.user.phone != 'لا يوجد'
                && advertisement.user.phone != '' && advertisement.user.phone != ' ' ? parseInt(advertisement.user.phone, 10) : '';

            var images = advertisement.images.map(a => a.url);

            var url = serviceURL + "share?id=" + advertisement.id;

            $scope.callWhatsLoad = true;

            window.plugins.socialsharing.canShareVia('whatsapp', 'msg', null, null, null, function (e) {
                window.open("whatsapp://send?text=&phone=" + contactPhone, '_system', 'location=no');
                setTimeout(function () {
                    $scope.callWhatsLoad = false;
                    $scope.$apply();
                }, 10);
            }, function (e) {
                $scope.callWhatsLoad = false;
                $scope.$apply();
                language.openFrameworkModal('خطأ', 'من فضلك قم بتحميل تطبيق الواتس أب أولا .', 'alert', function () { });
            });
        };

        $scope.ShareAdveritsementOnWhatsapp = function (advertisement) {
            if ($scope.shareWhatsLoad == true) return;

            var images = advertisement.images.map(a => a.url);

            //var mainImage = images.length > 0 ? images[0] : 'http://placehold.it/1000x400?text=NoImage';
            var mainImage = 'www/img/logo.png';

            var url = serviceURL + "share?id=" + advertisement.id;

            $scope.shareWhatsLoad = true;

            window.plugins.socialsharing.canShareVia('whatsapp', 'msg', null, null, null, function (e) {
                window.plugins.socialsharing.shareViaWhatsApp(advertisement.name, null, url, function () {
                    $scope.shareWhatsLoad = false;
                    $scope.$apply();
                }, function (errormsg) {
                    $scope.shareWhatsLoad = false;
                    $scope.$apply();
                    language.openFrameworkModal('خطأ', 'خطأ في المشاركة علي تطبيق الواتس .', 'alert', function () { });
                });

            }, function (e) {
                $scope.shareWhatsLoad = false;
                $scope.$apply();
                language.openFrameworkModal('خطأ', 'من فضلك قم بتحميل تطبيق الواتس أب أولا .', 'alert', function () { });
            });
        };

        $scope.ShareAdveritsementOnSocial = function (advertisement) {
            if ($scope.shareSocial == true) return;

            var images = advertisement.images.map(a => a.url);

            var mainImage = images.length > 0 ? images[0] : 'http://placehold.it/1000x400?text=NoImage';

            var url = serviceURL + "share?id=" + advertisement.id;

            $scope.shareSocial = true;

            var options = {
                message: advertisement.name,
                //subject: advertisement.name,
                //files: mainImage,
                url: url
            }

            window.plugins.socialsharing.shareWithOptions(options, function () {
                $scope.shareSocial = false;
                $scope.$apply();
            }, function (errormsg) {
                $scope.shareSocial = false;
                $scope.$apply();
                language.openFrameworkModal('خطأ', 'خطأ في المشاركة .', 'alert', function () { });
            });
        };

        $scope.ClickToOpenAdvertisementSettings = function (advertisement) {
            $scope.advertisementForPopup = advertisement;

            setTimeout(function () {
                var pOpenAdvertisementSetting = document.getElementById('pOpenAdvertisementSetting');
                $scope.$apply();
                app.popover('.popover-advertisement', pOpenAdvertisementSetting);
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.ChangeAdvertisementToSold = function (advertisement) {
            $scope.isAdvertisementChecked = true;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

            app.confirm('هل أنت متأكد من تغيير حالة الإعلان الي البيع ؟', 'تأكيد التغيير', function () {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                // app.showIndicator();

                appServices.CallService('advertisementsDetails', 'POST', 'api/v1/advertisement/status?advId=' + advertisement.id + "&&sold=1", '', function (result) {
                    app.hideIndicator();
                    if (result) {
                        $scope.isAdvertisementChecked = true;
                        $scope.status = 'تم البيع';
                        $scope.isSold = true;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });

            }, function () {
                $scope.isAdvertisementChecked = false;
                $scope.isSold = false;
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        };

        $scope.AdvertisementChanged = function (model) {
            $scope.isAdvertisementChecked = !model;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.openAdvertisementGallery = function (index) {
            myPhotoBrowserPopupDark.open(index);
        };

        $scope.UpdateAdvertisement = function (advertisement) {
            if (advertisement) {
                helpers.GoToPage('addAdvertisement', { advertisement: advertisement, isEdit: true });
            }
        };

        $scope.RemoveAdvertisement = function (advertisement) {
            app.confirm('هل أنت متأكد من حذف الإعلان ؟', 'تأكيد الحذف', function () {
                var categoryId = advertisement.category_id;
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                //app.showIndicator();

                appServices.CallService('advertisementsDetails', 'POST', 'api/v1/advertisement/delete?advId=' + advertisement.id, '', function (result) {
                    app.hideIndicator();
                    if (result) {
                        language.openFrameworkModal('نجاح', 'تم حذف الإعلان بنجاح .', 'alert', function () { });
                        helpers.GoToPage('advertisements', { methodName: 'api/v1/advertisement/category/list', categoryId: categoryId, children: result.children });
                    }

                    $scope.$apply();
                });

            }, function () {

            });
        };

        $scope.FollowAdvertisementComments = function (advertisement) {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    if ($scope.followAdvLoad == true) return;

                    var methodName = advertisement.isFollowed == true ? 'api/v1/unfollow?advId=' + advertisement.id : 'api/v1/follow?advId=' + advertisement.id;

                    $scope.followAdvLoad = true;

                    appServices.CallService('advertisementsDetails', 'POST', methodName, '', function (result) {
                        app.hideIndicator();
                        if (result) {
                            $scope.isUserFollowedBefore = methodName.indexOf('api/v1/follow') > -1 ? true : false;
                            $scope.advertisement.isFollowed = !$scope.advertisement.isFollowed;
                        }

                        $scope.followAdvLoad = false;
                        $scope.$apply();
                    });
                }
            });


        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToUserDetails = function (user) {
            helpers.GoToPage('userDetails', { user: user });
        };

        $scope.submitCommentForm = function (isValid, advertisement) {
            $scope.commentReset = true;
            $scope.submittedComment = true;
            if (isValid) {
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                // app.showIndicator();
                appServices.CallService('comments', "POST", "api/v1/comment/create?advId=" + advertisement.id + "&&comment=" + $scope.commentForm.comment, '', function (res2) {
                    app.hideIndicator();
                    if (res2 != null) {
                        $scope.commentForm.comment = undefined;
                        $scope.submittedComment = false;
                        $scope.commentReset = false;
                        language.openFrameworkModal('نجاح', 'تم إضافة تعليقك بنجاح .', 'alert', function () {
                        });

                        var comment = res2;
                        allComments.unshift(comment);
                        $scope.comments = allComments;
                        $scope.noComments = false;
                        var notificationParamter = {
                            api_token: res2.user.api_token,
                            advId: advertisement.id,
                            commentId: res2.id
                        }
                        appServices.CallService('comments', "POST", "api/v1/send/notifications/comment?api_token=" + res2.user.api_token, notificationParamter, function (notificationResult) {
                            if (notificationResult) {

                            }

                        });

                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            }
        };

        $scope.ItemIsLongTouched = function (comment) {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                    var IsUserLogged = typeof userLoggedIn != 'undefined' && userLoggedIn != null && userLoggedIn != '' && userLoggedIn != ' ';

                    if (IsUserLogged) {
                        comment.isCommentOwner = userLoggedIn.id == comment.user_id ? true : false;
                    }
                    else {
                        comment.isCommentOwner = false;
                    }

                    $scope.commentForPopup = comment;

                    setTimeout(function () {
                        $scope.$apply();
                        OpenCommentPopup(comment);
                    }, fw7.DelayBeforeScopeApply);
                }
            });
        };

        $scope.rateAdvertisement = function (rating, advertisement) {
            var params = {
                'advId': advertisement.id,
                'rateValue': rating,
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            //app.showIndicator();
            appServices.CallService('comments', "POST", "api/v1/rating", params, function (res2) {
                app.hideIndicator();
                if (res2 != null) {
                    var storedAdvertisement = JSON.parse(CookieService.getCookie('storedAdvertisement'));
                    advertisement.userRate = rating;
                    CookieService.setCookie('storedAdvertisement', JSON.stringify(storedAdvertisement));

                    language.openFrameworkModal('نجاح', 'تم تقييم الإعلان بنجاح .', 'alert', function () { });
                }
                else {
                    language.openFrameworkModal('خطأ', 'خطأ في تقييم الإعلان .', 'alert', function () { });
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        };

        $scope.GoToUpdateComment = function (comment, advertisement) {
            var commentModal = app.modal({
                title: 'تعديل التعليق',
                text: '',
                afterText: '<div class="list-block">' +
                '<div class="m-auto">' +
                '<ul>' +
                '<li>' +
                '<div class="item-content">' +
                '<div class="item-inner">' +
                '<div class="item-input">' +
                '<input id="txtComment" type="text" name="comment" placeholder="التعليق" value="' + comment.comment + '">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>',
                buttons: [
                    {
                        text: 'تعديل',
                        onClick: function () {
                            var commentText = $('#txtComment').val();

                            var params = {
                                'commentId': comment.id,
                                'comment': commentText
                            };

                            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                            // app.showIndicator();
                            appServices.CallService('comments', "POST", 'api/v1/comment/update', params, function (res) {
                                app.hideIndicator();
                                if (res != null) {
                                    language.openFrameworkModal('نجاح', 'تم تعديل التعليق بنجاح .', 'alert', function () { });
                                    angular.forEach(allComments, function (item) {
                                        if (item.id == comment.id) {
                                            item.comment = commentText;
                                        }
                                    });

                                    $scope.comments = allComments;
                                    $scope.$apply();
                                }
                                else {
                                    app.openModal(commentModal);
                                }
                            });
                        }
                    },
                    {
                        text: 'إلغاء',
                        onClick: function () {
                            app.closeModal(commentModal);
                        }
                    }
                ]
            });

            $("#txtComment").on("focus", function () {
                var element = document.getElementById('txtComment');
                var txtValue = $(this).val();
                element.focus();
                element.setSelectionRange(txtValue.length, txtValue.length);
            });
        };

        $scope.GoToReportComment = function (comment, advertisement) {
            var reportModal = app.modal({
                title: 'إبلاغ عن تعليق',
                text: '',
                afterText: '<div class="list-block">' +
                '<div class="m-auto">' +
                '<ul>' +
                '<li>' +
                '<div class="item-content">' +
                '<div class="item-inner">' +
                '<div class="item-input">' +
                '<input id="txtReport" type="text" name="comment" placeholder="أدخل بلاغك">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>',
                buttons: [
                    {
                        text: 'أرسال',
                        onClick: function () {
                            var reportText = $('#txtReport').val();

                            if (typeof reportText != 'undefined' && reportText != null && reportText != '' && reportText != ' ') {

                                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                                // app.showIndicator();

                                appServices.CallService('comments', 'POST', "api/v1/report?r_id=" + comment.id + "&&report=" + reportText + "&&type=-1", '', function (result) {
                                    app.hideIndicator();
                                    if (result != null) {
                                        language.openFrameworkModal('نجاح', 'تم الإبلاغ عن هذا التعليق بنجاح .', 'alert', function () { });
                                    }
                                });
                            }
                            else {
                                language.openFrameworkModal('تنبيه', 'من فضلك أدخل البلاغ أولا', 'alert', function () { });
                                app.openModal(reportModal);
                            }
                        }
                    },
                    {
                        text: 'إلغاء',
                        onClick: function () {
                            app.closeModal(reportModal);
                        }
                    }
                ]
            });
        };

        $scope.GoToRemoveComment = function (comment, advertisement) {
            app.confirm('هل أنت متأكد من حذف التعليق ؟', 'تأكيد الحذف', function () {
                var categoryId = advertisement.category_id;
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                // app.showIndicator();

                appServices.CallService('comments', 'POST', 'api/v1/comment/delete?commentId=' + comment.id, '', function (result) {
                    app.hideIndicator();
                    if (result) {
                        language.openFrameworkModal('نجاح', 'تم حذف التعليق بنجاح .', 'alert', function () { });
                        $scope.comments = $scope.comments.filter(function (item) {
                            return item.id !== comment.id;
                        });
                        allComments = $scope.comments;
                        $scope.noComments = allComments.length > 0 ? false : true;
                        if (allComments.length > 0) {
                            $$('#divNoComments').removeClass('partialNoResult');
                        }
                        else {
                            $$('#divNoComments"]').addClass('partialNoResult');
                        }
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });

            }, function () {

            });
        };

        $scope.resetForm = function () {
            $scope.submittedComment = false;
            $scope.commentReset = false;
            allComments = [];
            loadingComments = false;
            CookieService.setCookie('comments-page-number', 1);
            $scope.comments = null;
            app.pullToRefreshDone();
            $scope.commentForm.comment = undefined;
            if (typeof $scope.CommentForm != 'undefined' && $scope.CommentForm != null) {
                $scope.CommentForm.$setPristine(true);
                $scope.CommentForm.$setUntouched();
            }

            //setTimeout(function () {
            //    $scope.$apply();
            //}, fw7.DelayBeforeScopeApply);
        }

        app.init();
    });

}]);

