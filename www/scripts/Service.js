﻿/// <reference path="../js/angular.js" />

(function () {

    myApp.angular.factory('appServices', ['$http', '$rootScope', '$log', 'helpers', 'CookieService', '$exceptionHandler', '$interval', function ($http, $rootScope, $log, helpers, CookieService, $exceptionHandler, $interval) {
        'use strict';

        var CallService = function (pageName, CallType, MethodName, dataVariables, callBack) {
            var counter = 0;
            var options = { dimBackground: true };

            if (!$rootScope.load) {
                $rootScope.load = false;
                if ((pageName == 'home' && MethodName.indexOf('api/Country/GetCountries') > -1) || pageName != 'home') {
                }
                else {
                    $rootScope.load = false;
                }
            }

            if (messageInterval) {
                helpers.clearTimer();
            }


            if (MethodName.indexOf('api/v1/register') == -1 && MethodName.indexOf('api/v1/profile/update') == -1 || MethodName.indexOf('api/v1/advertisement/update') == -1 ||
                MethodName.indexOf('api/v1/advertisement/create') == -1) {
                messageInterval = $interval(function () {
                    counter++;
                    if (counter == 180) {
                        helpers.clearTimer();
                        language.openFrameworkModal('تنبيه', 'الخدمة غير متوفرة الآن بسبب بطء الإتصال بالإنترنت,من فضلك أعد المحاولة مرة أخري .', 'alert', function () {
                        });
                        myApp.fw7.views[0].router.loadPage({ pageName: 'connect' });
                    }
                }, 1000);
            }


            var contentType = "application/json";
            var token = CookieService.getCookie('appToken');
            var deviceId = CookieService.getCookie('deviceId');

            if (MethodName.indexOf('api/v1/password/change') > -1 || MethodName.indexOf('api/v1/profile') > -1 || MethodName.indexOf('api/v1/advertisement/category/list') > -1 ||
                MethodName.indexOf('api/v1/myadvs') > -1 || MethodName.indexOf('api/v1/favorites') > -1 || MethodName.indexOf('api/v1/advertisement/details') > -1 ||
                MethodName.indexOf('api/v1/comments/list') > -1 || MethodName.indexOf('api/v1/messages/list') > -1 || MethodName.indexOf('api/v1/conversations') > -1 ||
                MethodName.indexOf('api/v1/conversations/message') > -1 || MethodName.indexOf('api/v1/conversation/delete') > -1 ||
                MethodName.indexOf('api/v1/counts/list') > -1 || MethodName.indexOf('api/v1/notifications') > -1 || MethodName.indexOf('api/v1/notification/delete') > -1 ||
                MethodName.indexOf('api/v1/conversations/asread') > -1 || MethodName.indexOf('api/v1/markasread') > -1 || MethodName.indexOf('api/v1/conversation/offline') > -1 ||
                MethodName.indexOf('api/v1/devices/delete') > -1) {
                if (MethodName.indexOf('?') > -1) {
                    MethodName += '&api_token=' + token;
                }
                else {
                    MethodName += '?api_token=' + token;
                }
            }

            if (MethodName.indexOf('api/v1/login') > -1) {
                contentType = "application/x-www-form-urlencoded";
            }

            if (dataVariables != '' && dataVariables != null) {
                if (typeof deviceId != 'undefined' && deviceId != null) {
                    dataVariables.playerId = deviceId;
                }

                if (typeof token != 'undefined' && token != null && MethodName.indexOf('api/v1/login') == -1) {
                    dataVariables.api_token = token;
                }

                dataVariables = JSON.stringify(dataVariables);
            }
            else {
                if (typeof deviceId != 'undefined' && deviceId != null) {
                    dataVariables = { 'playerId': deviceId };
                }
                if (typeof token != 'undefined' && token != null && MethodName.indexOf('api/v1/login') == -1) {
                    dataVariables = { 'api_token': token };
                }
                dataVariables = dataVariables;
            }

            $http({
                method: CallType,
                url: serviceURL + MethodName,
                headers: {
                    "content-type": contentType,
                    "cache-control": "no-cache"
                },
                data: dataVariables,
                async: true,
                crossDomain: true,
            }).then(
                function successCallback(response) {
                    if (SpinnerPlugin) {
                        SpinnerPlugin.activityStop();
                    }

                    helpers.clearTimer();
                    if (typeof response.data.status != 'undefined' && response.data.status != null) {
                        if(response.data.status==401){
                            language.openFrameworkModal('خطأ', 'حسابك غير مفعل', 'alert', function () { });
                            helpers.GoToPage('login', null);    
                            CookieService.removeCookie('userLoggedIn')
                            callBack(null);
                            return
                        }

                        if (response.data.status == true || response.data.status == 'true' || response.data.status == 'success') {
                            if (typeof response.data.data != 'undefined' && response.data.data != null) {
                                callBack(response.data.data);
                            }
                            else if (typeof response.data != 'undefined' && response.data != null) {
                                callBack(response.data);
                            }
                            else if (typeof response.data != 'undefined' && response.data != null && typeof response.data.message != 'undefined' && response.data.message != null) {
                                callBack(response.data.message);
                            }
                            else if (typeof response.data.message.email != 'undefined' && response.data.message.email != null && response.data.message.email != '' && response.data.message.email.length > 0) {
                                language.openFrameworkModal('خطأ', response.data.message.email[0], 'alert', function () { });
                                callBack(true);
                            }
                            else {
                                if (response.data.message == 'Reset Code is invalid.') {
                                    language.openFrameworkModal('خطأ', 'الكود غير صحيح', 'alert', function () { });
                                }
                                else if (response.data.message == 'Old Password is incorrect') {
                                    language.openFrameworkModal('خطأ', 'كلمة السر القديمة غير صحيحة', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('خطأ', response.data.message, 'alert', function () { });
                                }
                                callBack(true);
                            }
                        }
                        else {
                            var message = response.data.message;
                            var errors = response.data.errors;

                            if (typeof message != 'undefined' && message != null) {
                                if (typeof response.data.message.email != 'undefined' && response.data.message.email != null && response.data.message.email != '' && response.data.message.email.length > 0) {
                                    language.openFrameworkModal('خطأ', response.data.message.email[0], 'alert', function () { });
                                    callBack(null);
                                }
                                else if (message == 'Old Password is incorrect') {
                                    language.openFrameworkModal('خطأ', 'كلمة السر القديمة غير صحيحة', 'alert', function () { });
                                    callBack(null);
                                }
                                else if (message == 'the email has been token.') {
                                    language.openFrameworkModal('خطأ', 'البريد الالكترونى مستخدم من قبل', 'alert', function () { });
                                    callBack(null);
                                }
                                else if (message == 'Reset Code is invalid.') {
                                    language.openFrameworkModal('خطأ', 'الكود غير صحيح', 'alert', function () { });
                                    callBack(null);
                                }
                                else if (message == 'your activation code is incorrect.' || message == 'code or phone incorrect') {
                                    callBack(null);
                                }
                                else if (typeof message != 'undefined' && message != null) {
                                    if (message == 'sorry, password not matched.') {
                                        callBack('passwordWrong');
                                    }
                                    else {
                                        language.openFrameworkModal('خطأ', message, 'alert', function () { });
                                        callBack(null);
                                    }
                                }
                            }
                            else {
                                if (typeof errors != 'undefined' && errors != null && errors != '' && errors != ' ') {
                                    if (typeof errors.activation_code != 'undefined' && errors.activation_code != null) {
                                        callBack(null);
                                    }
                                    if (typeof errors.phone != 'undefined' && errors.phone != null) {
                                        callBack('mobileExistBefore');
                                    }
                                    else {
                                        callBack(errors);
                                    }
                                }
                                else {

                                }
                            }

                        }
                    }
                    else {
                        language.openFrameworkModal('خطأ', 'Error In Service', 'alert', function () { });
                        callBack(null);
                    }
                },
                function errorCallback(error) {
                    if (SpinnerPlugin) {
                        SpinnerPlugin.activityStop();
                    }
                    var error = error;
                    if (error.status == 401) {
                        var message = error.data.data;
                        if (typeof error.data.message != 'undefined' && error.data.message != null) {
                            if (error.data.message == 'Sorry, Something is wrong.' || error.data.message == 'phone or password is incorrect' || error.data.message == 'sorry, password not matched.') {
                                language.openFrameworkModal('خطأ', 'رقم الجوال أو كلمة السر خاطئين', 'alert', function () { });
                            }
                            else if (error.data.message == 'Your Account not activated.') {
                                language.openFrameworkModal('خطأ', 'حسابك غير مفعل , من فضلك فعل حسابك', 'alert', function () { });
                                callBack('notActive');
                            }
                            else if (error.data.message == 'ur account suspend') {
                                language.openFrameworkModal('خطأ', 'حسابك قد تم حظره من قبل الأدمن , من فضلك تواصل مع الأدمن', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('خطأ', error.data.message, 'alert', function () { });
                            }
                        }
                        else {
                            if (typeof message != 'undefined' && message != null) {
                                language.openFrameworkModal('خطأ', message, 'alert', function () { });
                            }
                        }
                    }
                    else if (error.status == -1) {
                        callBack(null);
                    }
                    else if (error.status == 500) {
                        //language.openFrameworkModal('خطأ', 'Internal Server Error , Please Contact Application Support Team', 'alert', function () { });
                    }
                    else if (error.status == 400) {
                        var message = error.data.message;
                        if (error.data.data.new_phone != 'undefined' && error.data.data.new_phone != null && error.data.data.new_phone.length > 0) {
                            if (error.data.data.new_phone[0] == 'the new phone has been token.') {
                                callBack('mobileExistBefore');
                            }
                            else {
                                language.openFrameworkModal('خطأ', message, 'alert', function () { });
                                callBack(null);
                            }
                        }
                        else {
                            if (typeof message != 'undefined' && message != null) {
                                if (message == 'your phone is not correct.') {
                                    language.openFrameworkModal('خطأ', 'رقم الجوال الذي أدخلته غير صحيح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('خطأ', message, 'alert', function () { });
                                }
                            }
                        }
                    }
                    callBack(null);
                });
        }

        return {
            CallService: CallService
        }
    }]);

}());


