﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('homeController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loadingHome;
    var allQuestions = [];
    var isPull = true;
    var categoryId = 0;
    var homeSwiper;
    $rootScope.defaultCategories = [];
    var indexToGenerateAdv = 4;
    var isMessageChannelNBinded = false;
    var isCommentChannelNBinded = false;
    //$scope.Categories = new Array(20);
    //$scope.categoriesLoaded = true;
    $scope.defaultCategories = [];
    $scope.defaultImages = [];
    var watchID;
    var appToken;

    
    
    
    $scope.defaultImages.push(
        {  image: "http://souq-tubok.com/files/categories/1517400018.1517301884.ax2iSg3Gb88xFnAM1Jde_downloadfiles_wallpapers_3840_2160_chevrolet_camaro_muscle_car_17130.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1517400082.1517301809.QqxPDF4OraBQ15qAl4KHSingapore-top.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1519817576.kym-ellis-298554-unsplash.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1517400156.1517301224.PQIWJgV1AzczCYxMcQcbyumaconsole_hd.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1517400218.1517301416.DjQwosX6QHufcN4ui7x22.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1517399926.1517301433.aWOnIFRRAwfGsupJnYwh1.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1517399812.1517301589.JB0E7uCjYYZvJV5463WJ3.jpg" },
        {  image: "http://souq-tubok.com/files/categories/1517399782.1517301682.LN0qtCXCrM1W2iEDhSvNelectronic-appliances.jpg" },
    )

   
    LoadHomeData();
    getSponsorsAd()


    function ClearHistory() {
        for (var i = 0; i < fw7.views[0].history.length; i++) {
            if (fw7.views[0].history[i] === '#landing' || fw7.views[0].history[i] === '#login') fw7.views[0].history.splice(i, 1);
        }
    }

    function getSponsorsAd() {
        $rootScope.sponsors = [] 
        appServices.CallService('bankAccount', 'GET', "api/v1/general/info", '', function (result) {
          
                if(result.banners){
                    $rootScope.sponsors = result.banners;
                    $rootScope.initalHomeSwiper('#homeSwiper', $rootScope.sponsors)
                }
                else{
                    $rootScope.sponsors.push(
                        {  image: "http://souq-tubok.com/files/categories/1517400018.1517301884.ax2iSg3Gb88xFnAM1Jde_downloadfiles_wallpapers_3840_2160_chevrolet_camaro_muscle_car_17130.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1517400082.1517301809.QqxPDF4OraBQ15qAl4KHSingapore-top.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1519817576.kym-ellis-298554-unsplash.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1517400156.1517301224.PQIWJgV1AzczCYxMcQcbyumaconsole_hd.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1517400218.1517301416.DjQwosX6QHufcN4ui7x22.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1517399926.1517301433.aWOnIFRRAwfGsupJnYwh1.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1517399812.1517301589.JB0E7uCjYYZvJV5463WJ3.jpg" },
                        {  image: "http://souq-tubok.com/files/categories/1517399782.1517301682.LN0qtCXCrM1W2iEDhSvNelectronic-appliances.jpg" },
                    )

                    $rootScope.initalHomeSwiper('#homeSwiper', $rootScope.sponsors)
                }

        });
    }

    function CheckNotifications(callBack) {
        var notifications = [];
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        if (userLoggedIn) {

            appServices.CallService('home', "GET", 'api/v1/counts/list', '', function (res) {
                if (res != null) {
                    $rootScope.NewNotificationsNumber = parseInt(parseInt(res.notifyCount) + parseInt(res.messageCount));
                    $('#spanNewMessages').html(res.messageCount);
                    notifications = parseInt(parseInt(res.notifyCount) + parseInt(res.messageCount));
                }

                document.addEventListener("deviceready", function () {
                    cordova.plugins.notification.badge.set(notifications);
                }, false);

                callBack(notifications);

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        }
        else {
            $('.spanNotifications').hide();
            $scope.IsVisitor = true;
        }
    }

    function LoadCategories(callBack) {
        var categories = [];
        $scope.defaultCategories = [];

        if (localStorage.getItem('categories')) {
            $scope.local = true;
            $scope.defaultCategories = JSON.parse(localStorage.getItem('categories'));
            if (JSON.parse(localStorage.getItem('images'))) {
                $scope.defaultImages = [];
                $scope.defaultImages = JSON.parse(localStorage.getItem('images'))
            }

            if ($scope.defaultCategories.length > 0) {
                $scope.Categories = $scope.defaultCategories;
                categories = $scope.defaultCategories;
                myApp.fw7.Categories = $scope.defaultCategories;
                $$('.pull-to-refresh-layer').css('position', 'relative');
                $$('.page[data-page="home"]').removeClass('noResult');
                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
                navigator.splashscreen.hide();
                callBack($scope.defaultCategories);
            }
        }
        else {
            $scope.local = false;
            appServices.CallService('home', "GET", 'api/v1/categories', '', function (res) {
                $scope.categoriesLoaded = undefined;

                if (!isPull) {
                    app.hideIndicator();
                }
                if (res != null && res.length > 0) {
                    $scope.defaultImages = [];
                    angular.forEach(res, function (item) {
                        $scope.defaultImages.push(item.image)
                    });
                    localStorage.setItem('categories', JSON.stringify(res));
                    localStorage.setItem('images', JSON.stringify($scope.defaultImages));
                    $$('.pull-to-refresh-layer').css('position', 'relative');
                    $$('.page[data-page="home"]').removeClass('noResult');
                    $scope.Categories = res;
                    categories = res;
                    myApp.fw7.Categories = res;
                }
                else {
                    $$('.pull-to-refresh-layer').css('position', 'absolute');
                    $$('.page[data-page="home"]').addClass('noResult');
                }

                callBack(categories);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        }
      
        if (!isPull) {
            // SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            //app.showIndicator();
        }

       
    }

    function DrawMenu() {
        ClearHistory();

        var loginUsingSocial = CookieService.getCookie('loginUsingSocial') ? CookieService.getCookie('loginUsingSocial') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;
        var IsVisitor = userLoggedIn == null ? true : false;
        CookieService.setCookie('Visitor', IsVisitor);
        $scope.IsVisitor = IsVisitor;

        var fw7 = myApp.fw7;

        if (IsVisitor == true) {
            $('#linkMenuToHome').css('display', 'block ');
            $('#linkMenuToMyMessages').css('display', 'none');
            $('#spanNewMessages').css('display', 'none');
            $('#linkMenuToMyFavorites').css('display', 'none');
            $('#linkMenuToAppCommission').css('display', 'block');
            $('#linkMenuToBankAccounts').css('display', 'block');
            $('#linkMenuToTerms').css('display', 'block');
            $('#linkMenuToAboutUs').css('display', 'block');
            $('#linkMenuToChangePassword').css('display', 'none');
            $('#linkMenuToContact').css('display', 'none');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToProfile').css('display', 'none');
            $('#lblMenuLoginText').html('تسجيل دخول');
        }
        else {
            $('#linkMenuToHome').css('display', 'block');
            $('#linkMenuToMyMessages').css('display', 'block');
            $('#spanNewMessages').css('display', 'block');
            $('#linkMenuToMyFavorites').css('display', 'block');
            $('#linkMenuToAppCommission').css('display', 'block');
            $('#linkMenuToBankAccounts').css('display', 'block');
            $('#linkMenuToTerms').css('display', 'block');
            $('#linkMenuToAboutUs').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToProfile').css('display', 'block');
                if (loginUsingSocial == 'true') {
                    $('#linkMenuToChangePassword').css('display', 'none');
                }
                else {
                    $('#linkMenuToChangePassword').css('display', 'block');
                }
                $('#lblMenuLoginText').html('تسجيل خروج');
            }
            else {
                $('#linkMenuToProfile').css('display', 'none');
                $('#linkMenuToChangePassword').css('display', 'none');
                $('#lblMenuLoginText').html('تسجيل دخول');
            }

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                if (typeof userLoggedIn.image != 'undefined' && userLoggedIn.image != null && userLoggedIn.image != '' && userLoggedIn.image != ' ') {
                    if (userLoggedIn.image.indexOf('http://') > -1 || userLoggedIn.image.indexOf('https://') > -1 || userLoggedIn.image.indexOf('http://placehold.it') > -1 ||
                        userLoggedIn.image.indexOf('https://placehold.it') > -1) {
                        $$('#imgSideMenu').attr('src', userLoggedIn.image);
                    }
                    else {
                        if (userLoggedIn.image.indexOf('placehold.it') > -1) {
                            $$('#imgSideMenu').attr('src', 'http:' + userLoggedIn.image);
                        }
                        else {
                            $$('#imgSideMenu').attr('src', hostUrl + userLoggedIn.image);
                        }
                    }
                }
                else {
                    $$('#imgSideMenu').attr('src', 'img/logo.png');
                }
            }
        }
    }

    function ClearListData() {
        app.pullToRefreshDone();
    }

    function LoadHomeData() {
        $scope.defaultCategories = [];
        var userLoggedIn = null;
            var Visitor = CookieService.getCookie('Visitor')
        if(Visitor=="true"||Visitor==null){
            app.alert('الرجاء تسجيل الدخول للتطبيق للاستفادة من إضافة تعليق وإجراء محادثة ونشر اعلان', 'سوق تبوك');
        }
        CookieService.setCookie('introMessage', true);
       
        if (CookieService.getCookie('userLoggedIn')) {
            userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            console.log(userLoggedIn)
        }

        isPull = false;
        ClearListData();

        $$('.pull-to-refresh-arrow').hide();

        app.initPullToRefresh('#divInfiniteHome');

        $scope.isLoaded = false;

        document.addEventListener("deviceready", function () {

            if (userLoggedIn != null) {
                $rootScope.pusherSetup();
                $rootScope.CheckNewMessages(function (notifications) {
                    $scope.NewNotificationsNumber = notifications;
                    setTimeout(function () {
                        $scope.$apply();
                    }, 10);
                });
            }

            helpers.RegisterDevice(false, function (result) { });

            LoadCategories(function (result) {
                navigator.splashscreen.hide();
            });

            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'true' || IsVisitor == true) {
                $('.spanNotifications').hide();
                $scope.IsVisitor = true;
            }
            else {
                $scope.IsVisitor = false;
                CheckNotifications(function (notificationLength) {
                    document.addEventListener("deviceready", function () {
                        cordova.plugins.notification.badge.set(notificationLength);
                    }, false);

                    $scope.NewNotificationsNumber = notificationLength;
                    setTimeout(function () {
                        $scope.$apply();
                    }, 10);
                    $('.spanNotifications').show();
                    $('#elementNotificationsLoaded').show();
                    $('#elementNotificationsNotLoaded').hide();
                });
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);


            $$('#divInfiniteHome').on('ptr:refresh', function (e) {
                isPull = true;
                localStorage.removeItem('categories');
                localStorage.removeItem('images');
                var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
                if (IsVisitor == 'true' || IsVisitor == true) {
                    $('.spanNotifications').hide();
                    $scope.IsVisitor = true;
                }
                else {
                    $scope.IsVisitor = false;
                    $('#elementNotificationsLoaded').hide();
                    $('#elementNotificationsNotLoaded').show();
                    CheckNotifications(function (notificationLength) {
                        document.addEventListener("deviceready", function () {
                            cordova.plugins.notification.badge.set(notificationLength);
                        }, false);

                        $scope.NewNotificationsNumber = notificationLength;
                        setTimeout(function () {
                            $scope.$apply();
                        }, 10);
                        $('.spanNotifications').show();
                        $('#elementNotificationsLoaded').show();
                        $('#elementNotificationsNotLoaded').hide();
                    });
                }

                LoadCategories(function (result) {
                    app.pullToRefreshDone();
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

            DrawMenu();
        }, false);

    }

    $('#elementNotificationsLoaded').hide();
    $('#elementNotificationsNotLoaded').show();

    $(document).ready(function () {

        app.onPageInit('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
           
            DrawMenu();

            $$('.pull-to-refresh-arrow').hide();
        });

        app.onPageReinit('home', function (page) {
            $rootScope.currentOpeningPage = 'home';
    
            isPull = false;

            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'true' || IsVisitor == true) {
                $('.spanNotifications').hide();
                $scope.IsVisitor = true;
            }
            else {
                $scope.IsVisitor = false;
                CheckNotifications(function (notificationLength) {
                    document.addEventListener("deviceready", function () {
                        cordova.plugins.notification.badge.set(notificationLength);
                    }, false);
                    $scope.NewNotificationsNumber = notificationLength;
                    setTimeout(function () {
                        $scope.$apply();
                    }, 10);
                    $('.spanNotifications').show();
                    $('#elementNotificationsLoaded').show();
                    $('#elementNotificationsNotLoaded').hide();
                });
            }

            //LoadHomeData();

            $rootScope.RemoveEditPagesFromHistory();

            setTimeout(function () {
                $rootScope.$apply();
            }, 10);
        });

        app.onPageBeforeAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';
            if($rootScope.sponsors.length == 0 ||$rootScope.sponsors == undefined || $rootScope.sponsors == null  ){
                getSponsorsAd() 
            }
            //getSponsors()
            if ($rootScope.sponsors) {
                $rootScope.initalHomeSwiper('#homeSwiper', $rootScope.sponsors)
            }
        
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageBeforeAnimation('*', function (page) {
           // navigator.geolocation.clearWatch($rootScope.watchID);
    
        });
        app.onPageAfterAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';
           
            if (page.fromPage.name == 'signupUser' || page.fromPage.name == 'connect' || page.fromPage.name == 'login' || page.fromPage.name == 'landing') {
                LoadHomeData();
            }

        });



        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            $rootScope.notifyClick = true;
            cordova.plugins.notification.badge.clear();
            helpers.GoToPage('userNotification', null); 
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };

        $rootScope.CheckUserLoggedIn = function (callBack) {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'true') {
                app.confirm('يجب عليك تسجيل دخولك أولا , هل تود التسجيل ؟', 'تسجيل الدخول أولا', function () {
                    CookieService.removeCookie('appToken');
                    CookieService.removeCookie('USName');
                    CookieService.removeCookie('userLoggedIn');
                    CookieService.setCookie('Visitor', false);
                    helpers.GoToPage('login', null);
                    callBack(false);
                }, function () {
                    callBack(false);
                });
            }
            else {
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                if (typeof userLoggedIn.phone != 'undefined' && userLoggedIn.phone != null && userLoggedIn.phone != ' ' && userLoggedIn.phone != '') {
                    callBack(true);
                }
                else {
                    callBack(true);
                    //    app.confirm('يجب عليك تسجيل جوالك أولا , هل تود التسجيل ؟', 'تسجيل جوالك أولا', function () {
                    //        helpers.GoToPage('editProfile', { user: userLoggedIn });
                    //    }, function () {
                    //        callBack(false);
                    //    });
                    //}
                }
            }
        };

        $rootScope.CheckNotifications = function (callBack) {
            var notifications = [];
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            if (userLoggedIn) {

                appServices.CallService('home', "GET", 'api/v1/counts/list', '', function (res) {
                    if (res != null) {
                        $rootScope.NewNotificationsNumber = parseInt(parseInt(res.notifyCount) + parseInt(res.messageCount));
                        $('#spanNewMessages').html(res.messageCount);
                        notifications = parseInt(parseInt(res.notifyCount) + parseInt(res.messageCount));
                    }

                    document.addEventListener("deviceready", function () {
                        cordova.plugins.notification.badge.set(notifications);
                    }, false);

                    callBack(notifications);

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            }
            else {
                $('.spanNotifications').hide();
                $scope.IsVisitor = true;
            }
        };

        $rootScope.pusherSetup = function () {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            $rootScope.pusherEvents = pusherEvents;

            $rootScope.pusher = new Pusher('edbce55a046af674713e', {
                cluster: 'eu',
                encrypted: true
            });

            if (userLoggedIn) {
                $rootScope.channel = $rootScope.pusher.subscribe("tabuqChannel" + userLoggedIn.id);
                $rootScope.suspendChannel = $rootScope.pusher.subscribe("suspendChannel" + userLoggedIn.id);

                $rootScope.channel.bind('App\\Events\\AddNewComment', function (e) {
                    try {
                        $rootScope.NewNotificationsNumber = parseInt(parseInt(e.general.notifyCount) + parseInt(e.general.messageCount));
                        $('#spanNewMessages').html(e.general.messageCount);
                        setTimeout(function () {
                            $rootScope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    } catch (e) {
                        console.log(e)
                    }
                });

            }
        }

        $rootScope.CheckNewMessages = function (callBack) {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            if (userLoggedIn) {
                $rootScope.channel.bind('App\\Events\\AddNewComment', function (e) {
                    try {
                        $rootScope.NewNotificationsNumber = parseInt(parseInt(e.general.notifyCount) + parseInt(e.general.messageCount));
                        $('#spanNewMessages').html(e.general.messageCount);
                        $rootScope.notifyClick =false;
                        setTimeout(function () {
                            $rootScope.$apply();
                        }, fw7.DelayBeforeScopeApply);

                        callBack($rootScope.NewNotificationsNumber);
                    } catch (e) {
                        console.log(e)
                    }
                });

                $rootScope.channel.bind('App\\Events\\AddNewMessage', function (e) {
                    try {
                        $rootScope.NewNotificationsNumber = parseInt(parseInt(e.general.notifyCount) + parseInt(e.general.messageCount));
                        $('#spanNewMessages').html(e.general.messageCount);
                        $rootScope.notifyClick =false;

                        if ($rootScope.currentOpeningPage != 'chat') {
                            var textNotifications = ' المستخدم ';
                            textNotifications += e.user.name;
                            textNotifications += ' قام بإرسال رسالة : ';
                            textNotifications += e.data.message.message;

                            language.openFrameworkModal('نجاح', textNotifications, 'alert', function () { });
                        }
                        setTimeout(function () {
                            $rootScope.$apply();
                        }, 10);

                        callBack($rootScope.NewNotificationsNumber);
                    } catch (e) {
                        console.log(e)
                    }
                });

                $rootScope.suspendChannel.bind('App\\Events\\SuspendUser', function (e) {
                    try {
                        language.openFrameworkModal('نجاح', 'لقد تم إلغاء حسابك من قبل الأدمن', 'alert', function () {
                            cordova.plugins.notification.badge.clear();
                            CookieService.removeCookie('appToken');
                            CookieService.removeCookie('USName');
                            CookieService.removeCookie('userLoggedIn');
                            CookieService.removeCookie('deviceId');
                            CookieService.setCookie('Visitor', false);
                            helpers.GoToPage('login', null);
                        });
                    } catch (e) {
                        console.log(e)
                    }
                });

                callBack($rootScope.NewNotificationsNumber);
            }
        };

        $rootScope.RemoveEditPagesFromHistory = function () {
            for (var i = 0; i < fw7.views[0].history.length; i++) {
                if (fw7.views[0].history[i] === '#connect') fw7.views[0].history.splice(i, 1);
            }
        };

        $rootScope.isVisitor = function () {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'true') {
                return true;
            }
            else {
                return false;
            }
        };

        $scope.GoToAdvertisementsInCategory = function (category) {
            var categoriesStored = myApp.fw7.Categories;
            var children;
            var filteredCategory = categoriesStored.filter(function (cat) {
                return cat.id === category.id;
            })[0];
            helpers.GoToPage('advertisements', { methodName: 'api/v1/advertisement/category/list', categoryId: filteredCategory.id, children: filteredCategory.children });

            //appServices.CallService('home', "GET", 'api/v1/children/counts?categoryId=' + category.id, '', function (res) {
            //    if (res) {
            //        children = res;
            //        helpers.GoToPage('advertisements', { methodName: 'api/v1/advertisement/category/list', categoryId: filteredCategory.id, children: children });
            //    }
            //    else {
            //        helpers.GoToPage('advertisements', { methodName: 'api/v1/advertisement/category/list', categoryId: filteredCategory.id, children: filteredCategory.children });
            //    }
            //});
           
        };

       
        $rootScope.initalHomeSwiper = function (id,advertisments) {
            if ($rootScope.swiper)
                $rootScope.swiper.destroy(true, true);

                setTimeout(function(){
                    $rootScope.swiper = new Swiper(id, {
                        loop: false,
                        autoplayDisableOnInteraction: false,
                       spaceBetween: 0,
                        lazyLoading: true,
                        center: true,
                        preloadImages:true,
                        updateOnImagesReady:true,
                       // speed: 2000,
                        //slidesPerView: 1,
                        autoplay: 4000,
                    });
                   },300) ;    

        };

        app.init();
    });

}]);

