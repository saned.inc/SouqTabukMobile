﻿/// <reference path="../js/angular.js" />
myApp.angular.controller('messagesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingMessages;
    var allMessages = [];
    var isPull = true;

    function ClearListData() {
        allMessages = [];
        loadingMessages = false;
        CookieService.setCookie('messages-page-number', 1);
        $scope.messages = null;
        app.pullToRefreshDone();
    }

    function LoadMessages(callBack) {
        CookieService.setCookie('messages-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var params = {
            'page': parseInt(CookieService.getCookie('messages-page-number')),
            'show': myApp.fw7.PageSize
        };

        allMessages = [];
        app.attachInfiniteScroll('#divInfiniteMessages');

        if (!isPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            //app.showIndicator();
        }
        appServices.CallService('messages', 'GET', 'api/v1/messages/list', '', function (response) {
            if (!isPull) {
                app.hideIndicator();
            }

            allMessages = [];

            if (response && response.length > 0) {
                $$('.page[data-page="messages"]').removeClass('noResult');

                if (response.length < myApp.fw7.PageSize) {
                    $scope.messagesInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteMessages');
                }
                else {
                    $scope.messagesInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteMessages');
                }

                angular.forEach(response, function (message) {
                    message.isRead = message.read_at != null ? true : false;
                    allMessages.push(message);
                });

                $scope.messages = allMessages;
                $scope.noMessages = allMessages.length > 0 ? false : true;
            }
            else {
                $$('.page[data-page="messages"]').addClass('noResult');
                allMessages = [];
                $scope.messages = [];
                $scope.noMessages = true;
                $scope.messagesInfiniteLoader = false;
            }

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
                callBack(true);
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $('#MessagesNotificationsLoaded').hide();
    $('#MessagesNotificationsNotLoaded').show();

    $(document).ready(function () {
        app.onPageInit('messages', function (page) {
            if ($rootScope.currentOpeningPage != 'messages') return;
            $rootScope.currentOpeningPage = 'messages';

            $$('#divInfiniteMessages').on('ptr:refresh', function (e) {
                isPull = true;

                var IsVisitor = $rootScope.isVisitor();
                if (IsVisitor == true) {
                    $scope.IsVisitor = true;
                    $('.spanNotifications').hide();
                }
                else {
                    $scope.IsVisitor = false;
                    $('#MessagesNotificationsLoaded').hide();
                    $('#MessagesNotificationsNotLoaded').show();
                    $rootScope.CheckNotifications(function (notificationLength) {
                        $('#MessagesNotificationsLoaded').show();
                        $('#MessagesNotificationsNotLoaded').hide();
                    });
                }

                LoadMessages(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $$('#divInfiniteMessages').on('infinite', function () {
                if (loadingMessages) return;
                loadingMessages = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                CookieService.setCookie('messages-page-number', parseInt(CookieService.getCookie('messages-page-number')) + 1);

                var params = {
                    'page': parseInt(CookieService.getCookie('messages-page-number')),
                    'show': myApp.fw7.PageSize
                };

                appServices.CallService('messages', 'GET', 'api/v1/messages/list', '', function (response) {
                    if (response && response.length > 0) {
                        loadingMessages = false;

                        angular.forEach(response, function (message) {
                            message.isRead = message.read_at != null ? true : false;
                            allMessages.push(message);
                        });

                        $scope.messages = allMessages;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.messagesInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteMessages');
                            return;
                        }
                    }
                    else {
                        $scope.messagesInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteMessages');
                        loadingMessages = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('messages', function (page) {
            if ($rootScope.currentOpeningPage != 'messages') return;
            $rootScope.currentOpeningPage = 'messages';

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageReinit('messages', function (page) {

            isPull = false;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('messages', function (page) {
            if ($rootScope.currentOpeningPage != 'messages') return;
            $rootScope.currentOpeningPage = 'messages';

            isPull = false;
            $scope.isLoaded = false;

            app.initPullToRefresh('#divInfiniteMessages');

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            LoadMessages(function (result) { });

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#MessagesNotificationsLoaded').show();
                    $('#MessagesNotificationsNotLoaded').hide();
                });
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.GoToNotifications = function () {
            helpers.GoToPage('userNotification', null);
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };

        $scope.ItemIsLongTouched = function (message) {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'false') {
                app.confirm('هل أنت متأكد من حذف المحادثة ؟', 'تأكيد الحذف', function () {
                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    //app.showIndicator();

                    appServices.CallService('messages', 'POST', 'api/v1/conversation/delete?convId=' + message.id, '', function (result) {
                        app.hideIndicator();
                        if (result) {
                            language.openFrameworkModal('نجاح', 'تم حذف المحادثة بنجاح .', 'alert', function () { });
                            $scope.messages = $scope.messages.filter(function (item) {
                                return item.id !== message.id;
                            });
                            allMessages = $scope.messages;
                            $scope.messages = allMessages;
                            $scope.noMessages = allMessages.length > 0 ? false : true;
                            if (allMessages.length > 0) {
                                $$('.page[data-page="messages"]').removeClass('noResult');
                            }
                            else {
                                $$('.page[data-page="messages"]').addClass('noResult');
                            }
                        }

                        $scope.$apply();
                    });

                }, function () {

                });
            }
        };

        $scope.GoToChat = function (message) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
           // app.showIndicator();
           if(message.isRead){
            appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + message.id, '', function (chatMessages) {
                
                helpers.GoToPage('chat', { conversation_Id: message.id, adv_id: message.adv_id, messages: chatMessages });
            });
           }
           else{
            appServices.CallService('messages', 'POST', "api/v1/conversations/asread?convId=" + message.id, '', function (response) {
                app.hideIndicator();
                if (response) {
                    message.isRead = true;
                //     if (hasMessage == false) {
                        //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                        //  app.showIndicator();
                    //     appServices.CallService('advertisementsDetails', 'POST', 'api/v1/conversations?advId=' + message.adv_id, '', function (result) {
                    //         app.hideIndicator();
                    //         if (result && result != null) {
                    //             $scope.hasMessage = false;
                    //             advertisement.conversation_id = result.id;
                    //             appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + message.id, '', function (chatMessages) {
                    //                 setTimeout(function () {
                    //                     $scope.shareMessageLoad = false
                    //                     $scope.$apply();
                    //                 }, 100);
                    //                     helpers.GoToPage('chat', { conversation_Id: advertisement.conversation_id, adv_id: advertisement.id, messages: chatMessages });
                    //             });
                          
                    //         }
                    //     });
                    // }
                   // else {
                        appServices.CallService('chat', 'GET', "api/v1/conversations/message?convId=" + message.id, '', function (chatMessages) {
                            helpers.GoToPage('chat', { conversation_Id: message.id, adv_id: message.adv_id, messages: chatMessages });
                        });
                       
                    //}
             // helpers.GoToPage('chat', { conversation_Id: message.id, adv_id: message.advs.id });
                }
            });
           }           
        };

        app.init();
    });

}]);
