﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('advancedSearchController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $('#advancedSearchNotificationsLoaded').hide();
    $('#advancedSearchNotificationsNotLoaded').show();

    $(document).ready(function () {
        function LoadCities() {
            var cities = [];

            //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            //app.showIndicator();
            
   
            $('#linkAdvancedSearchCity .item-after').html('اختر مدينتك');
            $scope.searchAdvertisement.city = null;

            appServices.CallService('addAdvertisement', "GET", "api/v1/cities", '', function (res) {
                app.hideIndicator();
                if (res != null && res.length > 0) {
                    //$scope.searchAdvertisement.city = res[0].id;
                    res.splice(0, 1);
                    $scope.cities = res;
                    myApp.fw7.Cities = res;
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        }

        app.onPageBeforeAnimation('advancedSearch', function (page) {
            $scope.searchAdvertisement.title = undefined;
            $scope.searchAdvertisement.categotyId = undefined
            $('#linkAdvancedSearchCategory .item-after').html('التصنيف');
            $('#linkAdvancedSearchCategory select').val("");
            $('#linkAdvancedSearchCity .item-after').html('اختر مدينتك');
            $scope.searchAdvertisement.city = null;
            if ($rootScope.currentOpeningPage != 'advancedSearch') return;
            $rootScope.currentOpeningPage = 'advancedSearch';
            $scope.searchAdvertisement.title = undefined;
            $scope.searchAdvertisement.categotyId = undefined
            $('#linkAdvancedSearchCategory .item-after').html('التصنيف');
            $('#linkAdvancedSearchCategory select').val("");
        
            if (myApp.fw7.Categories.length>0) {
                $scope.categories = myApp.fw7.Categories
            }
        });


        app.onPageAfterAnimation('advancedSearch', function (page) {
            if ($rootScope.currentOpeningPage != 'advancedSearch') return;
            $rootScope.currentOpeningPage = 'advancedSearch';

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
            LoadCities();
            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#advancedSearchNotificationsLoaded').show();
                    $('#advancedSearchNotificationsNotLoaded').hide();
                });
            }
            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            $rootScope.RemoveEditPagesFromHistory();
            //$('#advancedSearchNotificationsLoaded').hide();
            //$('#advancedSearchNotificationsNotLoaded').show();
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
            
            //LoadUserData(userLoggedIn);
        });

        $scope.resetForm = function () {
            $scope.submittedContactUs = false;
            $scope.contactUsReset = false;
            $scope.contactUsForm.name = null;
            $scope.contactUsForm.email = null;
            $scope.contactUsForm.mobile = null;
            $scope.contactUsForm.content = null;
            if (typeof $scope.ContactUsForm != 'undefined' && $scope.ContactUsForm != null) {
                $scope.ContactUsForm.$setPristine(true);
                $scope.ContactUsForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (isValid) {
            $scope.searchReset = true;
            $scope.submittedSearch = true;
            if (isValid) {
                var categoryId = $scope.searchAdvertisement.categotyId;
                var adTitle = $scope.searchAdvertisement.title;
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
              
                //app.showIndicator();
                appServices.CallService('activation', "GET", "api/v1/advertisement/search?categoryId=" + categoryId + "&adName=" + adTitle + "&cityId=" + $scope.searchAdvertisement.city+"&pageSize=200&page=1", "", function (res) {
                    app.hideIndicator();
                    if (res != null && res.data.length>0) {
                        helpers.GoToPage('advertisements', { pageName: "search", searchResults: res.data, categoryId: categoryId, adTitle: adTitle });
                    }
                    else {
                        language.openFrameworkModal('نجاح', 'لايوجد اعلانات', 'alert', function () { });
                    }
                });
            }
        };

        $scope.form = {};
        $scope.searchAdvertisement = {};

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        $scope.OpenSocial = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('نجاح', 'رابط التواصل الإجتماعي غير صحيح .', 'alert', function () { });
            }
        };

        $scope.GoToHome = function () {
            helpers.GoToPage('home', null);
        };

        $scope.GoToSearch = function () {
             helpers.GoToPage('advancedSearch', null);
        };

        $scope.GoToNotifications = function () {
            $scope.NewNotificationsNumber = 0;
            helpers.GoToPage('userNotification', null); $rootScope.notifyClick = true;    cordova.plugins.notification.badge.clear();
        };

        $scope.GoToAdvertisement = function () {
            $rootScope.CheckUserLoggedIn(function (result) {
                if (result == true) {
                    helpers.GoToPage('addAdvertisement', null);
                }
            });
        };
      
        app.init();
    });

}]);

